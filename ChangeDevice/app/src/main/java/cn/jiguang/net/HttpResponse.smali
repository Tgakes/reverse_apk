.class public Lcn/jiguang/net/HttpResponse;
.super Ljava/lang/Object;
.source "HttpResponse.java"


# instance fields
.field e:J

.field g:Z


# direct methods
.method public constructor <init>()V
    .registers 5

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x493e0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcn/jiguang/net/HttpResponse;->e:J

    .line 18
    return-void
.end method

.method private printTime(J)V
    .registers 6
    .param p1, "time"    # J

    .prologue
    .line 36
    const-string v0, "logs"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "time===="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    return-void
.end method


# virtual methods
.method public queryDisplayName()V
    .registers 3

    .prologue
    .line 25
    const/4 v1, 0x0

    .line 26
    .local v1, "s":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_4} :catch_5

    .line 32
    :goto_4
    return-void

    .line 28
    :catch_5
    move-exception v0

    .line 29
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4
.end method
