package qhb.wjdiankong.hookpms;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.cmi.jegotrip2.call.model.CalledStatus;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * im
 */
public class CheckService extends Service {


    private final IBinder mBinder = new ImSocketBinder();

    Handler mHandler;

    boolean isCheck;

    long lastTime = 0;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    CheckListener listener;

    public void setCallBack(CheckListener listener) {
        this.listener = listener;
        force();
        //10秒检测一次
    }


    public void force() {

        if (mHandler == null) {
            mHandler = new Handler();
            if (listener != null) {
                CalledStatus calledStatus = listener.getCallStatus();
                isCheck = (calledStatus != null && !"2".equals(calledStatus.cs_forward));
            }
        }
        checkTime();
        mHandler.postDelayed(runnable, 1000 * 60);
    }


    private void checkTime() {
        long currentTime = System.currentTimeMillis();
        if (lastTime == 0) {
            lastTime = currentTime;
        } else {
            if (currentTime - lastTime >= 30 * 1000 * 60) {//30分钟重新登录一次
                lastTime = currentTime;
                if (listener != null) {
                    CalledStatus calledStatus = listener.getCallStatus();
                    if (calledStatus != null && "2".equals(calledStatus.cs_forward)) {
                        listener.reLogin();
                    }
                }
            }
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// HH:mm:ss
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        Log.i("logs", "Date获取当前日期时间" + simpleDateFormat.format(date));
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            checkTime();
            Log.i("logs", "isCheck===" + isCheck + ",listener====" + listener);
            if (isCheck) {
                if (listener != null) {
                    CalledStatus calledStatus = listener.getCallStatus();
                    if (calledStatus != null) {
                        Log.i("logs", "calledStatus===" + calledStatus.toString());
                        if (!"2".equals(calledStatus.cs_forward)) {
                            isCheck = false;
                            listener.startOpen();
                        }
                    } else {
                        Log.i("logs", "calledStatus is null===");
                    }
                }
            }
            force();
        }
    };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        startRunTimer();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {

        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacks(runnable);
            mHandler.removeCallbacksAndMessages(null);
        }

//        screenReceiverUtils.stopScreenReceiverListener();
    }


    public class ImSocketBinder extends Binder {


        public CheckService getService() {

            return CheckService.this;
        }
    }


    public interface CheckListener {

        CalledStatus getCallStatus();

        void startOpen();

        void reLogin();
    }

}
