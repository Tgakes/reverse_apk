package qhb.wjdiankong.hookpms;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.cmi.jegotrip.application.SysApplication;
import com.cmi.jegotrip.callmodular.functionActivity.TrusteeshipStateActivity;
import com.cmi.jegotrip.callmodular.justalk.realm.RealmManager;
import com.cmi.jegotrip.ui.LoginUserInfoUtils;
import com.cmi.jegotrip.ui.login2.PhoneLoginByPasswordActivity;
import com.cmi.jegotrip.util.LocalSharedPrefsUtil;
import com.cmi.jegotrip.util.ScreenActivityManager;
import com.cmi.jegotrip2.call.model.CalledStatus;

public class ControlQiHooApplication extends SysApplication {


    private static ControlQiHooApplication instance = null;


    public static synchronized ControlQiHooApplication getInstance() {
        ControlQiHooApplication sysApplication;
        synchronized (ControlQiHooApplication.class) {
            if (instance == null) {
                instance = new ControlQiHooApplication();
            }
            sysApplication = instance;
        }
        return sysApplication;
    }

    @Override
    public void onCreate() {
        CrashHandler.getInstance().init();
        super.onCreate();
    }

    CheckService checkService;

    public void startService(Activity activity) {

        Intent intent = new Intent(activity, CheckService.class);
        activity.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public void stopService(Activity activity) {
        activity.unbindService(mConnection);
    }

    public void setCheckServiceStatus() {
        if (checkService != null) {
            checkService.setCheck(true);
        }
    }


    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {

            // We've bound to LocalService, cast the IBinder and get LocalService instance
            CheckService.ImSocketBinder binder = (CheckService.ImSocketBinder) service;
            checkService = binder.getService();
            startListener();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {

        }
    };

    private void startListener() {

        if (checkService != null) {
            checkService.setCallBack(new CheckService.CheckListener() {
                @Override
                public CalledStatus getCallStatus() {

                    Activity activity = ScreenActivityManager.getAppManager().currentActivity();
                    Log.i("logs", "activity===" + activity);
                    return isLogin() ? LocalSharedPrefsUtil.getCalledStatus(activity != null ? activity : SysApplication.getInstance()) : null;
                }

                @Override
                public void startOpen() {

                    if (isLogin()) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Activity activity = ScreenActivityManager.getAppManager().currentActivity();
//                                Log.i("logs","activity==="+(activity != null && (activity instanceof TrusteeshipStateActivity)));
                                if (activity != null && (activity instanceof TrusteeshipStateActivity)) {
                                    TrusteeshipStateActivity activity1 = (TrusteeshipStateActivity) activity;
                                    activity1.clickOpenTrusteeship();
                                } else {
                                    Intent localIntent = new Intent(activity != null ? activity : SysApplication.getInstance(), TrusteeshipStateActivity.class);
                                    localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    (activity != null ? activity : SysApplication.getInstance()).startActivity(localIntent);
                                }

                            }
                        });
                    }

                }

                @Override
                public void reLogin() {
                    Log.i("gakes","reLogin===1");
                    Activity activity = ScreenActivityManager.getAppManager().currentActivity();
                    Context context;

                    if ((context = activity) != null || (context = SysApplication.getInstance()) != null) {
                        RealmManager.removeAllMessages();
                        PhoneLoginByPasswordActivity.start1(context, "+86", "中国", LoginUserInfoUtils.getInstance(context).getPhoneNumber(), LoginUserInfoUtils.getInstance(context).getPwd());
                    }
                    Log.i("gakes","context==="+context);
                }
            });
        }
    }
}
