package tg;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myapplication.R;

public class AgreementDialogUtils {


    private static void setBg(Activity activity, View view, int color) {

        int round = CommonUtils.dp2px(activity, 10f);
        Drawable normalDrawable = CommonUtils.getGradientDrawable(new float[]{round, round, round, round, round, round, round, round}, color, CommonUtils.resourceToColor(android.R.color.transparent, activity), 0);
        CommonUtils.setBackground(view, normalDrawable);
    }


    public static void showAgreementDialogs(final Activity activity, final Runnable runnable) {

        int paddings = CommonUtils.dp2px(activity, 15f);
        LinearLayout llRoot = new LinearLayout(activity);
        llRoot.setPadding(paddings, paddings, paddings, paddings);
        llRoot.setOrientation(LinearLayout.VERTICAL);
        setBg(activity, llRoot, CommonUtils.resourceToColor(android.R.color.white, activity));
//        LinearLayout llRoot = (LinearLayout) LayoutInflater.from(activity).inflate(R.layout.agreement_dialog,  null);

        TextView tvContent = new TextView(activity);
        tvContent.setText("用户协议与隐私政策概要");
        tvContent.setTextSize(18f);
        tvContent.setTextColor(CommonUtils.resourceToColor(android.R.color.black, activity));
        tvContent.setGravity(Gravity.CENTER);
        llRoot.addView(tvContent);

        //lineSpacingMultiplier

        tvContent = new TextView(activity);
        tvContent.setPadding(0, CommonUtils.dp2px(activity, 20f), 0, 0);
        tvContent.setText("我们将会遵循隐私政策收集和使用信息，帮助您了解我们收集、存储和使用个人信息的情况，以及我们所采集的个人信息类型与应用途径。\n为了方便您能正常使用我们所提供的功能或服务，请你允许应用访问您的设备存储等权限，我们不会在功能或服务不需要时通过您授权的权限收集您的个人信息资料。");
        tvContent.setTextSize(14f);
        tvContent.setTextColor(CommonUtils.resourceToColor(android.R.color.black, activity));
        tvContent.setLineSpacing(0, 1.2f);
        llRoot.addView(tvContent);

        TextView tvInfo = new TextView(activity);
        tvInfo.setTextSize(14f);
        tvInfo.setLineSpacing(0, 1.2f);
        tvInfo.setPadding(0, 0, 0, CommonUtils.dp2px(activity, 20f));
        tvInfo.setTextColor(CommonUtils.resourceToColor(android.R.color.black, activity));
        llRoot.addView(tvInfo);


        int padding = CommonUtils.dp2px(activity, 20f);
        TextView tvConfirm = new TextView(activity);
        tvConfirm.setText("同意并继续使用");
        setBg(activity, tvConfirm, Color.parseColor("#ff5985df"));
        tvConfirm.setTextSize(16f);
        tvConfirm.setPadding(padding, padding / 2, padding, padding / 2);
        tvConfirm.setTextColor(CommonUtils.resourceToColor(android.R.color.white, activity));
        tvConfirm.setGravity(Gravity.CENTER);
        llRoot.addView(tvConfirm);


        padding = CommonUtils.dp2px(activity, 15f);
        TextView tvCancel = new TextView(activity);
        tvCancel.setText("不同意");
        tvCancel.setTextSize(16f);
        tvCancel.setPadding(padding, padding, padding, padding);
        tvCancel.setTextColor(CommonUtils.resourceToColor(android.R.color.darker_gray, activity));
        tvCancel.setGravity(Gravity.CENTER);
        llRoot.addView(tvCancel);

//        View inflate = LayoutInflater.from(activity).inflate(R.layout.agreement_dialog,  null);
//        TextView textView = (TextView) inflate.findViewById(R.id.tv_info);


        final AlertDialog create = new AlertDialog.Builder(activity, R.style.Theme_AppCompat_Light_Dialog_Alert).setView(llRoot).create();
        create.setCanceledOnTouchOutside(false);
        create.setCancelable(false);
        create.show();

        Window dialogWindow = create.getWindow();
        WindowManager m = activity.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        // 设置宽度
        p.width = (int) (d.getWidth() * 0.95); // 宽度设置为屏幕的0.95
        p.gravity = Gravity.CENTER;//设置位置
        p.height = d.getHeight() / 2;
        //p.alpha = 0.8f;//设置透明度
        dialogWindow.setAttributes(p);

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append("您需要同意《用户协议》和《隐私政策》才能使用本应用。");
        spannableStringBuilder.setSpan(new ClickableSpan() {

            public void onClick(View view) {
                gotoActivity(activity, true);
            }
        }, 5, 11, 34);
        spannableStringBuilder.setSpan(new ClickableSpan() {

            public void onClick(View view) {
                gotoActivity(activity, false);
            }
        }, 12, 18, 34);
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.parseColor("#F82900"));
        ForegroundColorSpan foregroundColorSpan2 = new ForegroundColorSpan(Color.parseColor("#F82900"));
        spannableStringBuilder.setSpan(foregroundColorSpan, 5, 11, 34);
        spannableStringBuilder.setSpan(foregroundColorSpan2, 12, 18, 34);
        tvInfo.setText(spannableStringBuilder);
        tvInfo.setMovementMethod(LinkMovementMethod.getInstance());


        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                create.dismiss();
                SpUtils.getInstance(activity.getApplicationContext()).saveFirst(false);
                if (runnable != null) {
                    runnable.run();
                }

            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                create.dismiss();
                activity.finish();
            }
        });
    }


    private static void gotoActivity(Activity activity, boolean isAgreement) {

        Intent intent = new Intent(activity, AgreementActivity.class);
        intent.putExtra("isAgreement", isAgreement);
        activity.startActivity(intent);
    }

}
