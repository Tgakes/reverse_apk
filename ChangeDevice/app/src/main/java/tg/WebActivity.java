package tg;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.myapplication.R;

public class WebActivity extends Activity {


    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String url = getIntent().getStringExtra("url");
        LinearLayout llRoot = new LinearLayout(this);
        llRoot.setOrientation(LinearLayout.VERTICAL);
        setContentView(llRoot);
//        LinearLayout llRoot = findViewById(R.id.parent);
//        ViewGroup.LayoutParams lp = llRoot.getLayoutParams();
//        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
//        lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
//        llRoot.setLayoutParams(lp);


        int[] colors = {Color.parseColor("#ff1d86d0"), Color.parseColor("#ff8d5bfd")};
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        drawable.setCornerRadius(0);
        drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        int topHeight = CommonUtils.dp2px(this, 44f);
        RelativeLayout rlTop = new RelativeLayout(this);
        CommonUtils.setBackground(rlTop, drawable);
        llRoot.addView(rlTop);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rlTop.getLayoutParams();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = topHeight;
        rlTop.setLayoutParams(params);

        int paddings = CommonUtils.dp2px(this, 5f);
        TextView tvBack = new TextView(this);
        rlTop.addView(tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvBack.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.mipmap.ic_white_back), null, null, null);
        tvBack.setPadding(paddings, paddings, 0, paddings);
        RelativeLayout.LayoutParams rlParams = (RelativeLayout.LayoutParams) tvBack.getLayoutParams();
        rlParams.width = (int) (topHeight * 1.5);
        rlParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        tvBack.setLayoutParams(rlParams);

        TextView tvContent = new TextView(this);
        rlTop.addView(tvContent);
        tvContent.setText("详情");
        tvContent.setTextSize(18f);
        tvContent.setTextColor(getResources().getColor(android.R.color.white));
        tvContent.setGravity(Gravity.CENTER);
        rlParams = (RelativeLayout.LayoutParams) tvContent.getLayoutParams();
        rlParams.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        rlParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        rlParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        tvContent.setLayoutParams(rlParams);


        WebView webView = new WebView(this);

        /**
         * 通过此WebView 获取到 WebSettings ，通过WebSettings设置WebView
         */
        WebSettings webSettings = webView.getSettings();
        /**
         * 设置支持JavaScript激活，可用等
         */
        webSettings.setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setBlockNetworkImage(false);
        webSettings.setUseWideViewPort(true);
        /**
         * 设置自身浏览器，注意：可用把WebView理解为浏览器，设置new WebViewClient()后，手机就不会跳转其他的浏览器
         */
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                Log.e("logs", " SslError=" + error);
                //证书信任
                handler.proceed();
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //支持重定向
                if(!(url.startsWith("http")||url.startsWith("https"))){
                    return true;
                }
                return false;
            }
        });

        webView.setVerticalScrollBarEnabled(true);


        llRoot.addView(webView);
        params = (LinearLayout.LayoutParams) webView.getLayoutParams();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        webView.setLayoutParams(params);

        webView.loadUrl(url);


    }


}
