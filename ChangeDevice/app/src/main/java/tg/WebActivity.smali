.class public Ltg/WebActivity;
.super Landroid/app/Activity;
.source "WebActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 24
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    .prologue
    .line 35
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual/range {p0 .. p0}, Ltg/WebActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    const-string v18, "url"

    invoke-virtual/range {v17 .. v18}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 37
    .local v14, "url":Ljava/lang/String;
    new-instance v6, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 38
    .local v6, "llRoot":Landroid/widget/LinearLayout;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 39
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Ltg/WebActivity;->setContentView(Landroid/view/View;)V

    .line 47
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v4, v0, [I

    const/16 v17, 0x0

    const-string v18, "#ff1d86d0"

    invoke-static/range {v18 .. v18}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v18

    aput v18, v4, v17

    const/16 v17, 0x1

    const-string v18, "#ff8d5bfd"

    invoke-static/range {v18 .. v18}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v18

    aput v18, v4, v17

    .line 48
    .local v4, "colors":[I
    new-instance v5, Landroid/graphics/drawable/GradientDrawable;

    sget-object v17, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    move-object/from16 v0, v17

    invoke-direct {v5, v0, v4}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 49
    .local v5, "drawable":Landroid/graphics/drawable/GradientDrawable;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 50
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/GradientDrawable;->setGradientType(I)V

    .line 52
    const/high16 v17, 0x42300000    # 44.0f

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Ltg/CommonUtils;->dp2px(Landroid/content/Context;F)I

    move-result v11

    .line 53
    .local v11, "topHeight":I
    new-instance v10, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 54
    .local v10, "rlTop":Landroid/widget/RelativeLayout;
    invoke-static {v10, v5}, Ltg/CommonUtils;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 55
    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 57
    invoke-virtual {v10}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout$LayoutParams;

    .line 58
    .local v8, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v17, -0x1

    move/from16 v0, v17

    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 59
    iput v11, v8, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 60
    invoke-virtual {v10, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    const/high16 v17, 0x40a00000    # 5.0f

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Ltg/CommonUtils;->dp2px(Landroid/content/Context;F)I

    move-result v7

    .line 63
    .local v7, "paddings":I
    new-instance v12, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 64
    .local v12, "tvBack":Landroid/widget/TextView;
    invoke-virtual {v10, v12}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 65
    new-instance v17, Ltg/WebActivity$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ltg/WebActivity$1;-><init>(Ltg/WebActivity;)V

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    invoke-virtual/range {p0 .. p0}, Ltg/WebActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0b0002

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 72
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v7, v7, v0, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 73
    invoke-virtual {v12}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 74
    .local v9, "rlParams":Landroid/widget/RelativeLayout$LayoutParams;
    int-to-double v0, v11

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3ff8000000000000L    # 1.5

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 75
    const/16 v17, -0x1

    move/from16 v0, v17

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 76
    invoke-virtual {v12, v9}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    new-instance v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 79
    .local v13, "tvContent":Landroid/widget/TextView;
    invoke-virtual {v10, v13}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 80
    const-string v17, "\u8be6\u60c5"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    const/high16 v17, 0x41900000    # 18.0f

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 82
    invoke-virtual/range {p0 .. p0}, Ltg/WebActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x106000b

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getColor(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 83
    const/16 v17, 0x11

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 84
    invoke-virtual {v13}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .end local v9    # "rlParams":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 85
    .restart local v9    # "rlParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v17, -0x2

    move/from16 v0, v17

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 86
    const/16 v17, -0x2

    move/from16 v0, v17

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 87
    const/16 v17, 0xd

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 88
    invoke-virtual {v13, v9}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    new-instance v16, Landroid/webkit/WebView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 96
    .local v16, "webView":Landroid/webkit/WebView;
    invoke-virtual/range {v16 .. v16}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v15

    .line 100
    .local v15, "webSettings":Landroid/webkit/WebSettings;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 101
    sget v17, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v18, 0x15

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_151

    .line 102
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    .line 104
    :cond_151
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 105
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 106
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    .line 107
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 111
    new-instance v17, Ltg/WebActivity$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ltg/WebActivity$2;-><init>(Ltg/WebActivity;)V

    invoke-virtual/range {v16 .. v17}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 128
    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 131
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 132
    invoke-virtual/range {v16 .. v16}, Landroid/webkit/WebView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .end local v8    # "params":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v8, Landroid/widget/LinearLayout$LayoutParams;

    .line 133
    .restart local v8    # "params":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v17, -0x1

    move/from16 v0, v17

    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 134
    const/16 v17, -0x1

    move/from16 v0, v17

    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 135
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 137
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 140
    return-void
.end method
