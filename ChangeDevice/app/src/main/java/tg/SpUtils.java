package tg;

import android.content.Context;
import android.content.SharedPreferences;

public class SpUtils {


    private final static String REG_USER_INFO_NAME = "spUtils_t";

    private final static String IS_FIRST = "isFirst";


    private static SpUtils singleton;

    private static SharedPreferences sharedPreferences;

    public static synchronized SpUtils getInstance(Context context) {
        if (singleton == null) {
            synchronized (SpUtils.class) {
                if (singleton == null) {
                    singleton = new SpUtils();
                }
                singleton.setContext(context);
            }
        } else {
            singleton.setContext(context);
        }

        return singleton;

    }

    Context context;

    public Context getContext() {

        return context;
    }

    public void setContext(Context context) {
        if (context != null) {
            this.context = context;
            if (sharedPreferences == null) {
                sharedPreferences = context.getSharedPreferences(REG_USER_INFO_NAME, Context.MODE_PRIVATE);
            }
        }
    }



    public void saveFirst(boolean isFirst) {
        SharedPreferences sp = getSp(); //私有数据
        if (sp == null) {
            return;
        }
        SharedPreferences.Editor editor = sp.edit();//获取编辑器
        editor.putBoolean(IS_FIRST, isFirst);
        editor.apply();//提交修改
    }


    public boolean isFirst() {
        SharedPreferences share = getSp(); //私有数据
        if (share == null) {
            return true;
        }
        return share.getBoolean(IS_FIRST, true);
    }


    private SharedPreferences getSp() {

        if (sharedPreferences != null) {
            return sharedPreferences;
        }

        if (context != null) {
            return context.getSharedPreferences(REG_USER_INFO_NAME, Context.MODE_PRIVATE);
        }
        return null;
    }

}
