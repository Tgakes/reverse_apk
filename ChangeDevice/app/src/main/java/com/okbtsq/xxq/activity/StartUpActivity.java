package com.okbtsq.xxq.activity;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;



public class StartUpActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private void showAgreement() {

        boolean isEnter = true;
        if (SpUtils.getInstance(this).isReg()) {

            if (TmacUtils.isNetworkAvailable(this)) {
                TmacUtils.requestEnter(this, new Runnable() {

                    @Override
                    public void run() {
                        showAgreement1();
                    }
                });
            } else {
                showAgreement1();
            }
        } else {
            TmacUtils.killProcess(this);
        }
    }

    private void showAgreement1() {

    }
}
