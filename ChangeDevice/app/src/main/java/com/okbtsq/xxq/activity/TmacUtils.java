
package com.okbtsq.xxq.activity;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/**
 * 3DES加密工具类
 */
public class TmacUtils {

//    public final static String DOMAIN = "http://47.241.13.103:8011/wyx_api";
    public final static String DOMAIN = "http://appapi.pcb.boxzhi.com/index/other/test";

    private final static String KEY = "12d401d784e4429492efaeed535f1342";

    // 密钥 取前24位
    private final static String SECRET_KEY = KEY.substring(0, 24);
    // 向量 取前8位
    private final static String IV = SECRET_KEY.substring(0, 8);
    // 加解密统一使用的编码方式
    private final static String encoding = "utf-8";




    /**
     * 向指定URL发送GET方法的请求
     *
     * @param url   发送请求的URL
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     */
    public static String sendGet(String url, String param) {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + param;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            connection.setConnectTimeout(15 * 1000);//30s
            connection.setReadTimeout(15 * 1000);  //30s
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                Log.i("logs",key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            Log.e("logs","发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }






    public static void requestEnter(final Activity activity,final Runnable runnable) {

        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //发送 POST 请求
                    String str = sendGet(DOMAIN, "platform=okbtbsq");
                    Log.i("logs", "enter get请求结果：" + str);
                    boolean isCl = true;
                    if (!TextUtils.isEmpty(str)) {
                        if ("1".equals(str)) {
                            isCl = false;
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    runnable.run();
                                }
                            });
                        }else if ("0".equals(str)){
                            SpUtils.getInstance(activity).saveReg(false);
                        }

                    }

                    if (isCl) {
                        killProcess(activity);
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void killProcess(Activity activity){
        activity.finish();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }


    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager manager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();

    }




}
