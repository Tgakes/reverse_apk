package com.test;

public class WashFaceFilter extends AbstractPrepareFilter implements StudyPrepareFilter{

    public WashFaceFilter(AbstractPrepareFilter nextPrepareFilter) {
        super(nextPrepareFilter);
    }

    public WashFaceFilter() {
    }

    @Override
    public void prepare(PreparationList preparationList) {
        if (preparationList.isWashFace()) {
            System.out.println("已经洗过脸");
        }
    }

    @Override
    public void doFilter(PreparationList preparationList, FilterChain filterChain) {
        if (preparationList.isWashFace()) {
            System.out.println("已经洗过脸");
        }
        filterChain.doFilter(preparationList,filterChain);
    }
}
