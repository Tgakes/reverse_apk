package com.test;

//责任链模式
public class Main {

   public void testResponsibility(){

       PreparationList preparationList = new PreparationList();
       preparationList.setWashFace(true);
       preparationList.setWashHair(false);
       preparationList.setHaveBreakfast(true);

       Study study = new Study();

       HaveBreakfastFilter haveBreakfastFilter = new HaveBreakfastFilter(null);
       WashFaceFilter washFaceFilter = new WashFaceFilter(haveBreakfastFilter);
       WashHairFilter washHairFilter = new WashHairFilter(washFaceFilter);

       washHairFilter.doFilter(preparationList,study);
   }

   public void testResponsibilityAdvance(){

       PreparationList preparationList = new PreparationList();
       preparationList.setWashFace(true);
       preparationList.setWashHair(false);
       preparationList.setHaveBreakfast(true);

       Study study = new Study();

       StudyPrepareFilter washFaceFilter = new WashFaceFilter();
       StudyPrepareFilter washHairFilter = new WashHairFilter();
       StudyPrepareFilter haveBreakfastFilter = new HaveBreakfastFilter();


       FilterChain filterChain = new FilterChain(study);
       filterChain.addFilter(washFaceFilter);
       filterChain.addFilter(washHairFilter);
       filterChain.addFilter(haveBreakfastFilter);
       filterChain.doFilter(preparationList, filterChain);
   }

}
