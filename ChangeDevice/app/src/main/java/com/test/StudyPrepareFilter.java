package com.test;

public interface StudyPrepareFilter {

     void doFilter(PreparationList preparationList, FilterChain filterChain);
}
