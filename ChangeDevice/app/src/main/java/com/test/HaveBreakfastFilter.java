package com.test;

public class HaveBreakfastFilter extends AbstractPrepareFilter implements StudyPrepareFilter {


    public HaveBreakfastFilter(AbstractPrepareFilter nextPrepareFilter) {
        super(nextPrepareFilter);
    }

    public HaveBreakfastFilter() {
    }

    @Override
    public void prepare(PreparationList preparationList) {
        if (preparationList.isHaveBreakfast()) {
            System.out.println("已经吃过早餐");
        }
    }

    @Override
    public void doFilter(PreparationList preparationList, FilterChain filterChain) {
        if (preparationList.isHaveBreakfast()) {
            System.out.println("已经吃过早餐");
        }
        filterChain.doFilter(preparationList,filterChain);
    }
}
