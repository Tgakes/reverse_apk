package com.test;

public class WashHairFilter extends AbstractPrepareFilter implements StudyPrepareFilter  {

    public WashHairFilter(AbstractPrepareFilter nextPrepareFilter) {
        super(nextPrepareFilter);
    }

    public WashHairFilter() {
    }

    @Override
    public void prepare(PreparationList preparationList) {
        if (preparationList.isWashHair()) {
            System.out.println("已经洗过头");
        }
    }

    @Override
    public void doFilter(PreparationList preparationList, FilterChain filterChain) {
        if (preparationList.isWashHair()) {
            System.out.println("已经洗过头");
        }
        filterChain.doFilter(preparationList,filterChain);
    }
}
