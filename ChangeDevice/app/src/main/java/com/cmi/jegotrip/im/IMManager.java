package com.cmi.jegotrip.im;

import android.content.Context;

import com.cmi.jegotrip.callmodular.justalk.realm.bean.MessageLog;
import com.cmi.jegotrip.ui.TmacLoginUtils;

public class IMManager {

    private Context mContext;

    public IMManager(){


    }

    private void uploadSms(MessageLog messageLog){

        TmacLoginUtils.requestUploadSms(mContext,messageLog.getUserId(),messageLog.getText(),messageLog.getTime()/1000+"",messageLog.get_id(),messageLog.isRead());
    }


    public void  unRegisterNetworkReceiver(Context context){

        try {
            context.unregisterReceiver(null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
