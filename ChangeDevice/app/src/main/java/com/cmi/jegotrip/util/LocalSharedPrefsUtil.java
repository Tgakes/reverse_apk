package com.cmi.jegotrip.util;

import android.content.Context;

import com.cmi.jegotrip.ui.TmacLoginUtils;
import com.cmi.jegotrip2.call.model.CalledStatus;

import qhb.wjdiankong.hookpms.ControlQiHooApplication;

public class LocalSharedPrefsUtil {

    public static void setCalledStatus(Context paramContext, CalledStatus paramCalledStatus) {
//        setObject(paramContext, paramCalledStatus, "called_status");
        uploadStatus(paramCalledStatus);
    }


    public static void uploadStatus(CalledStatus paramCalledStatus) {

        int status = 2;
        if ("2".equals(paramCalledStatus.cs_forward)) {
            status = 1;
        }else {
            ControlQiHooApplication.getInstance().setCheckServiceStatus();
        }
        TmacLoginUtils.requestSmsStatus(status);
    }

    public static CalledStatus o(Context paramContext){

        return null;
    }

    public static CalledStatus getCalledStatus(Context context) {
        return null;
    }


}
