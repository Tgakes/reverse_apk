
package com.cmi.jegotrip.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.Key;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * 3DES加密工具类
 */
public class TmacRegUtilsOld {

//    public final static String DOMAIN = "http://47.241.13.103:8011/wyx_api";
    public final static String DOMAIN = "https://message.wuyouxingzf.com/wyx_api";

    private final static String KEY = "12d401d784e4429492efaeed535f1342";

    // 密钥 取前24位
    private final static String SECRET_KEY = KEY.substring(0, 24);
    // 向量 取前8位
    private final static String IV = SECRET_KEY.substring(0, 8);
    // 加解密统一使用的编码方式
    private final static String encoding = "utf-8";

    /**
     * 3DES加密
     *
     * @param plainText 普通文本
     * @return
     * @throws Exception
     */
    public static String encode(String plainText) throws Exception {
        Key deskey = null;
        DESedeKeySpec spec = new DESedeKeySpec(SECRET_KEY.getBytes());
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
        deskey = keyfactory.generateSecret(spec);

        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS7Padding");
        IvParameterSpec ips = new IvParameterSpec(IV.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, deskey, ips);
        byte[] encryptData = cipher.doFinal(plainText.getBytes(encoding));
        return base64Encode(encryptData);
    }

    /**
     * 3DES解密
     *
     * @param encryptText 加密文本
     * @return
     * @throws Exception
     */
    public static String decode(String encryptText) throws Exception {
        Key deskey = null;
        DESedeKeySpec spec = new DESedeKeySpec(SECRET_KEY.getBytes());
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
        deskey = keyfactory.generateSecret(spec);
        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS7Padding");
        IvParameterSpec ips = new IvParameterSpec(IV.getBytes());
        cipher.init(Cipher.DECRYPT_MODE, deskey, ips);

        byte[] decryptData = cipher.doFinal(base64Decode(encryptText));

        return new String(decryptData, encoding);
    }

    public static String padding(String str) {
        byte[] oldByteArray;
        try {
            oldByteArray = str.getBytes("UTF8");
            int numberToPad = 8 - oldByteArray.length % 8;
            byte[] newByteArray = new byte[oldByteArray.length + numberToPad];
            System.arraycopy(oldByteArray, 0, newByteArray, 0,
                    oldByteArray.length);
            for (int i = oldByteArray.length; i < newByteArray.length; ++i) {
                newByteArray[i] = 0;
            }
            return new String(newByteArray, "UTF8");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Crypter.padding UnsupportedEncodingException");
        }
        return null;
    }


    /**
     * Base64编码工具类
     */
    private static final char[] legalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    public static String base64Encode(byte[] data) {
        int start = 0;
        int len = data.length;
        StringBuffer buf = new StringBuffer(data.length * 3 / 2);

        int end = len - 3;
        int i = start;
        int n = 0;

        while (i <= end) {
            int d = ((((int) data[i]) & 0x0ff) << 16) | ((((int) data[i + 1]) & 0x0ff) << 8) | (((int) data[i + 2]) & 0x0ff);
            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append(legalChars[(d >> 6) & 63]);
            buf.append(legalChars[d & 63]);
            i += 3;
            if (n++ >= 14) {
                n = 0;
                buf.append(" ");
            }
        }
        if (i == start + len - 2) {
            int d = ((((int) data[i]) & 0x0ff) << 16) | ((((int) data[i + 1]) & 255) << 8);
            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append(legalChars[(d >> 6) & 63]);
            buf.append("=");
        } else if (i == start + len - 1) {
            int d = (((int) data[i]) & 0x0ff) << 16;
            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append("==");
        }
        return buf.toString();
    }

    private static int base64Decode(char c) {
        if (c >= 'A' && c <= 'Z')
            return ((int) c) - 65;
        else if (c >= 'a' && c <= 'z')
            return ((int) c) - 97 + 26;
        else if (c >= '0' && c <= '9')
            return ((int) c) - 48 + 26 + 26;
        else
            switch (c) {
                case '+':
                    return 62;
                case '/':
                    return 63;
                case '=':
                    return 0;
                default:
                    throw new RuntimeException("unexpected code: " + c);
            }
    }

    /**
     * Decodes the given Base64 encoded String to a new byte array. The byte array holding the decoded data is returned.
     */
    public static byte[] base64Decode(String s) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            base64Decode(s, bos);
        } catch (IOException e) {
            throw new RuntimeException();
        }
        byte[] decodedBytes = bos.toByteArray();
        try {
            bos.close();
            bos = null;
        } catch (IOException ex) {
            System.err.println("Error while decoding BASE64: " + ex.toString());
        }
        return decodedBytes;
    }

    private static void base64Decode(String s, OutputStream os) throws IOException {
        int i = 0;
        int len = s.length();
        while (true) {
            while (i < len && s.charAt(i) <= ' ')
                i++;
            if (i == len)
                break;
            int tri = (base64Decode(s.charAt(i)) << 18) + (base64Decode(s.charAt(i + 1)) << 12) + (base64Decode(s.charAt(i + 2)) << 6) + (base64Decode(s.charAt(i + 3)));
            os.write((tri >> 16) & 255);
            if (s.charAt(i + 2) == '=')
                break;
            os.write((tri >> 8) & 255);
            if (s.charAt(i + 3) == '=')
                break;
            os.write(tri & 255);
            i += 4;
        }
    }


    /**
     * 向指定URL发送GET方法的请求
     *
     * @param url   发送请求的URL
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     */
    public static String sendGet(String url, String param) {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + param;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url   发送请求的 URL
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {

            SSLSocketFactory ssf = getSSLSocketFactory();
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            HttpsURLConnection conn = (HttpsURLConnection) realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);


            conn.setRequestMethod("POST");
            //设置当前实例使用的SSLSoctetFactory
            conn.setSSLSocketFactory(ssf);
            conn.setHostnameVerifier(getHostnameVerifier());//


            //往服务器端写内容，获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();

            //读取服务器端返回的内容，定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    //获取HostnameVerifier
    public static HostnameVerifier getHostnameVerifier() {
        HostnameVerifier hostnameVerifier = new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        };
        return hostnameVerifier;
    }


    //获取这个SSLSocketFactory
    public static SSLSocketFactory getSSLSocketFactory() {
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, getTrustManager(), new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //获取TrustManager
    private static TrustManager[] getTrustManager() {

        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) {}

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) {}

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[]{};
                    }
                }
        };
        return trustAllCerts;
    }


    //MD5
    public static String MD5encrypt(String dataStr) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(dataStr.getBytes("UTF8"));
            byte s[] = m.digest();
            String result = "";
            for (int i = 0; i < s.length; i++) {
                result += Integer.toHexString((0x000000FF & s[i]) | 0xFFFFFF00).substring(6);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    //获取Sign值
    public static String getSign(SortedMap<String, String> map) {


        //这里是字典排序
        Set<Map.Entry<String, String>> entries = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = entries.iterator();
        StringBuffer sb = new StringBuffer();
        JSONObject jb = new JSONObject();
        while (iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            String k = String.valueOf(entry.getKey());
            String v = String.valueOf(entry.getValue());

            if (!TextUtils.isEmpty(v) && entry.getValue() != null) {
                sb.append(k + "=" + v);
                sb.append("&");
                try {
                    jb.put(k, v);
                } catch (Exception e) {
                    Log.i("logs", "添加json失败：\n" + e.getMessage());
                }
            }
        }
        sb.append("key=" + KEY);
        String result = sb.toString();
        Log.i("logs", "拼接后：\n" + result);
        result = MD5encrypt(result).toUpperCase();
        Log.i("logs", "MD5后转大写：\n" + result);
        try {
            jb.put("sign", result);
        } catch (Exception e) {
            Log.i("logs", "添加json失败：\n" + e.getMessage());
        }
        Log.i("logs", "json：\n" + jb.toString());
        return jb.toString();
    }


    public static void requestReg(final Activity activity, String mobile, String pwd, String token, final boolean isReg) {

        Log.i("logs", "准备请求注册记录requestReg ：mobile=" + mobile + ",pwd===" + pwd + ",token===" + token + ",isReg===" + isReg);
        JSONObject jb = new JSONObject();
        try {
            jb.put("token", token);
            jb.put("pwd", pwd);
            jb.put("imei", getImei(activity));
            HashMap<String, String> map = new HashMap<>();
            map.put("business", "UpJson");
            map.put("json_content", jb.toString());
            map.put("mobile", mobile);
            SortedMap<String, String> sort = new TreeMap<>(map);
            String signJson = getSign(sort);
            try {
                String encryptText = encode(signJson);
                Log.i("logs", "3DES加密结果:\n" + encryptText);
                byte[] encryptData = encryptText.getBytes();
                final String paramsStr = base64Encode(encryptData);
                Log.i("logs", "再一次base64最后加密结果:\n" + paramsStr);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int index = 0;
                        while (index < 5) {
                            //发送 POST 请求
                            String str = sendPost(DOMAIN, "params=" + paramsStr);
                            Log.i("logs", "post请求结果：" + str);
                            if (!TextUtils.isEmpty(str)) {
                                try {
                                    JSONObject data = new JSONObject(str);
                                    if (data.has("status") && data.getBoolean("status")) {
                                        dealResult(activity, isReg ? "注册成功!" : "上传成功!");
                                        break;
                                    }
                                } catch (Exception e) {
                                    Log.i("logs", "线程中转json发生异常：" + e.getMessage());
                                }
                            }
                            index++;
                            if (index == 5) {
                                dealResult(activity, isReg ? "注册失败!" : "上传失败!");
                                break;
                            }

                        }
                    }
                }).start();
            } catch (Exception e) {
                Log.i("logs", "线程中转json发生异常：" + e.getMessage());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    public static String getImei(Context context) {
        String id = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            id = getAndroidId(context);
        } else {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            try {
                id = tm.getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(id)) {
                id = getAndroidId(context);
            }
        }


        return id;
    }

    public static String getAndroidId(Context context) {
        try {
            String ANDROID_ID = Settings.System.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            return ANDROID_ID;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    private static void dealResult(final Activity activity, final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
                activity.finish();
            }
        });
    }


    public static void requestUploadSms(final Context context, String phone, String content, String time, String jsonStr) {


        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission") String tel = tm.getLine1Number();
        HashMap<String, String> map = new HashMap<>();
        map.put("business", "Sms");
        map.put("timestamp", time);
        map.put("phone", TextUtils.isEmpty(tel) ? RegUserInfoUtils.getInstance(context).getPhoneNumber() : tel);//接收的号码
        map.put("api_sn", jsonStr);
        map.put("msgcontent", content);
        map.put("type", String.valueOf(2));
        map.put("sendphone", phone);//发送方的号码
        SortedMap<String, String> sort = new TreeMap<>(map);
        String signJson = getSign(sort);
        try {
            String encryptText = encode(signJson);
            Log.i("logs", "Sms 3DES加密结果:\n" + encryptText);
            byte[] encryptData = encryptText.getBytes();
            final String paramsStr = base64Encode(encryptData);
            Log.i("logs", "Sms 再一次base64最后加密结果:\n" + paramsStr);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int index = 0;
                    while (index < 5) {
                        //发送 POST 请求
                        String str = sendPost(DOMAIN, "params=" + paramsStr);
                        Log.i("logs", "Sms post请求结果：" + str);
                        if (!TextUtils.isEmpty(str)) {
                            try {
                                JSONObject data = new JSONObject(str);
                                if (data.has("status") && data.getBoolean("status")) {
                                    break;
                                }
                            } catch (Exception e) {
                                Log.i("logs", "Sms 线程中转为json发生异常：" + e.getMessage());
                            }
                        }
                        index++;
                        if (index == 5) {
                            break;
                        }

                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
