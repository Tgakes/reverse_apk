package com.cmi.jegotrip.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.cmi.jegotrip.application.SysApplication;
import com.cmi.jegotrip.util.ScreenActivityManager;

public class LoginUserInfoUtils {


//     intent.putExtra("area_code", str);
//        intent.putExtra(RoamingCountry.COUNTRY_NAME, str2);
//        intent.putExtra("phone_number", str3);
//        intent.putExtra("pwd", str4);


    private final static String LOGIN_USER_INFO_NAME = "TGLoginUserInfo";
    private final static String AREA_CODE = "area_code";
    private final static String COUNTRY_NAME = "country_name";
    private final static String PHONE_NUMBER = "phone_number";
    private final static String PWD = "pwd";
    private final static String YD_SDK_TOKEN = "ydSdkToken";
    private final static String SECURITY_PHONE = "securityPhone";
    private final static String IMEI_CODE = "imeiCode";
    private final static String SAVE_PATH_CODE = "savePathCode";

    private final static String DOMAIN = "domain";

    private final static String SMS_API_ADDRESS = "smsApiAddress";



    private static LoginUserInfoUtils singleton;

    private static SharedPreferences sharedPreferences;

    public static synchronized LoginUserInfoUtils getInstance(Context context) {
        if (singleton == null) {
            synchronized (LoginUserInfoUtils.class) {
                if (singleton == null) {
                    singleton = new LoginUserInfoUtils();
                }
                singleton.setContext(context);
            }
        } else {
            singleton.setContext(context);
        }

        return singleton;

    }


    Context context;

    public void setContext(Context context) {
        if (context != null) {
            this.context = context;
        }
         context = (context == null ? (SysApplication.getInstance() == null ? ScreenActivityManager.b().a() : SysApplication.getInstance()) : context);
        if (sharedPreferences == null && context != null) {
            sharedPreferences = context.getSharedPreferences(LOGIN_USER_INFO_NAME, Context.MODE_PRIVATE);
        }
    }

    //String areaCode, String countryName, String phoneNumber, String pwd
    public void save(String ydSdkToken, String phoneNumber,String pwd,String imei) {
        SharedPreferences sharedPreferences = getSp(); //私有数据
        if (sharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
//        editor.putString(AREA_CODE, areaCode);
//        editor.putString(COUNTRY_NAME, countryName);
//        editor.putString(PHONE_NUMBER, phoneNumber);
//        editor.putString(PWD, pwd);
        editor.putString(YD_SDK_TOKEN, ydSdkToken);
        editor.putString(PHONE_NUMBER, phoneNumber);
        editor.putString(PWD, pwd);
        editor.putString(IMEI_CODE, imei);
        editor.commit();//提交修改
    }


    public void saveDomainInfo(String domain, String smsAddress) {
        SharedPreferences sharedPreferences = getSp(); //私有数据
        if (sharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
        editor.putString(DOMAIN, domain);
        editor.putString(SMS_API_ADDRESS, smsAddress);
        editor.commit();//提交修改
    }

    public String getDomain() {
        SharedPreferences share = getSp();
        if (share == null) {
            return "";
        }
        return share.getString(DOMAIN, "");
    }

    public String getSmsApiAddress() {
        SharedPreferences share = getSp();
        if (share == null) {
            return "";
        }
        return share.getString(SMS_API_ADDRESS, "");
    }


    public void setSavePathCode(int savePathCode) {
        SharedPreferences sharedPreferences = getSp(); //私有数据
        if (sharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
        editor.putInt(SAVE_PATH_CODE, savePathCode);
        editor.commit();//提交修改
    }
    //



    public int getSavePathCode() {
        SharedPreferences share = getSp();
        if (share == null) {
            return 0;
        }
        return share.getInt(SAVE_PATH_CODE, 0);
    }

    public String getYdSdkToken() {
        SharedPreferences share = getSp();
        if (share == null) {
            return "";
        }
        return share.getString(YD_SDK_TOKEN, "");
    }

    public String getPhoneNumber() {
        SharedPreferences share = getSp();
        if (share == null) {
            return "";
        }
        return share.getString(PHONE_NUMBER, "");
    }

    public String getImeiCode() {
        SharedPreferences share = getSp();
        if (share == null) {
            return "";
        }
        String imei = share.getString(IMEI_CODE, "");
        Log.i("logs", "get imei====" + imei);
        return imei;
    }

    public String getPwd() {
        SharedPreferences share = getSp();
        if (share == null) {
            return "";
        }
        return share.getString(PWD, "");
    }

    private SharedPreferences getSp() {
        Log.i("logs", "sharedPreferences====" + sharedPreferences);
        if (sharedPreferences != null) {
            return sharedPreferences;
        }


        Log.i("logs", "this.context===" + this.context + ",SysApplication.getInstance====" + SysApplication.getInstance() + ",ScreenActivityManager.getAppManager().currentActivity()====" + ScreenActivityManager.b().a());
        Context context = (this.context == null ? (SysApplication.getInstance() == null ? ScreenActivityManager.b().a() : SysApplication.getInstance()) : this.context);
        if (context != null) {
            return context.getSharedPreferences(LOGIN_USER_INFO_NAME, Context.MODE_PRIVATE);
        }
        return null;
    }

}
