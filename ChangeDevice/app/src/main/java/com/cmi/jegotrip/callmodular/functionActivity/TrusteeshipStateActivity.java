package com.cmi.jegotrip.callmodular.functionActivity;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cmi.jegotrip.util.LocalSharedPrefsUtil;
import com.cmi.jegotrip2.call.model.CalledStatus;

import org.json.JSONException;
import org.json.JSONObject;


public class TrusteeshipStateActivity extends Activity {



    boolean isCanRequest = false;

    TextView currentState;

    CalledStatus mCalledStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TrusteeshipStateActivity.showOpenFailToast(this,new JSONObject());
    }


   public void clickOpenTrusteeship(){
       this.mCalledStatus = LocalSharedPrefsUtil.o(this);
       if (this.mCalledStatus != null && "2".equals(this.mCalledStatus.cs_forward)) {
           return;
       }
       openOrCloseTrusteeship(this.currentState);
   }

   public static void showOpenFailToast(TrusteeshipStateActivity activity,JSONObject data){
       if (data != null) {
           try {
               String msg;
               if (!"0".equals(data.getString("code")) && !TextUtils.isEmpty((msg=data.getString("msg")))) {
                  Toast.makeText(activity,msg,Toast.LENGTH_LONG).show();
               }
           } catch (JSONException e) {
               e.printStackTrace();
           }
       }
   }

    public void openOrCloseTrusteeship(View paramView){

        if (isCanRequest) {
            setProgressBarIndeterminate(true);
        }

    }


    public void showSuccessDialog(){
        Toast.makeText(this,"号码托管功能开启失败!",Toast.LENGTH_LONG).show();
//        Intent intent = new Intent(this, MessageListActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//        finish();
    }
}
