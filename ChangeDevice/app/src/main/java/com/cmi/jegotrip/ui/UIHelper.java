package com.cmi.jegotrip.ui;

import android.content.Context;
import android.content.Intent;



public class UIHelper {

    public static void login(Context context) {
        Intent intent = new Intent(context, WelcomeLoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("phone",LoginUserInfoUtils.getInstance(context).getPhoneNumber());
        intent.putExtra("pwd",LoginUserInfoUtils.getInstance(context).getPwd());
        context.startActivity(intent);
    }

    public static void loginNotData(Context context) {
        Intent intent = new Intent(context, WelcomeLoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }



}
