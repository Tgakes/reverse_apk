package com.cmi.jegotrip.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cmi.jegotrip.application.SysApplication;
import com.cmi.jegotrip.ui.login2.PhoneLoginByPasswordActivity;

import java.util.ArrayList;
import java.util.List;

public class WelcomeLoginActivity extends Activity {

    Button loginButton;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout root = new LinearLayout(this);
        root.setFocusable(true);
        root.setFocusableInTouchMode(true);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        root.setGravity(Gravity.CENTER);

        TextView tvVersion = new TextView(this);
        tvVersion.setText("当前打包的版本日期：2020.12.25");
        tvVersion.setTextColor(Color.parseColor("#333333"));
        tvVersion.setTextSize(18f);
        root.addView(tvVersion, params);

        int dp13 = dp2px(13f);
        editText = new EditText(this);
        editText.setHint("请输入手机号");
        editText.setInputType(EditorInfo.TYPE_CLASS_PHONE);
        editText.setPadding(dp13, dp13, dp13, dp13);
        editText.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        final String phone = getIntent().getStringExtra("phone");
        String saveAddress;
        if (!TextUtils.isEmpty(phone)) {
            editText.setText(phone);
        } else if (!TextUtils.isEmpty((saveAddress = LoginUserInfoUtils.getInstance(WelcomeLoginActivity.this).getPhoneNumber()))) {
            editText.setText(saveAddress);
        }
        root.addView(editText, params);


        final EditText etInfo = new EditText(this);
        etInfo.setHint("请输入获取手机号信息接口");
        etInfo.setInputType(EditorInfo.TYPE_CLASS_TEXT);
        etInfo.setPadding(dp13, dp13, dp13, dp13);
        root.addView(etInfo, params);
        if (!TextUtils.isEmpty((saveAddress = LoginUserInfoUtils.getInstance(WelcomeLoginActivity.this).getDomain()))) {
            etInfo.setText(saveAddress);
        }

        final EditText etUploadSms = new EditText(this);
        etUploadSms.setHint("请输入上报短信接口");
        etUploadSms.setInputType(EditorInfo.TYPE_CLASS_TEXT);
        etUploadSms.setPadding(dp13, dp13, dp13, dp13);
        root.addView(etUploadSms, params);
        if (!TextUtils.isEmpty((saveAddress = LoginUserInfoUtils.getInstance(WelcomeLoginActivity.this).getSmsApiAddress()))) {
            etUploadSms.setText(saveAddress);
        }
        loginButton = new Button(this);
        loginButton.setText("获取信息");
        loginButton.setPadding(dp13, dp13, dp13, dp13);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tel = editText.getText().toString().trim();
                if (!TextUtils.isEmpty(tel)) {
                    if (tel.length() != 11) {
                        Toast.makeText(WelcomeLoginActivity.this, "请输入正确的手机号", Toast.LENGTH_LONG).show();
                        return;
                    }

                    String info = etInfo.getText().toString().trim();
                    if (TextUtils.isEmpty(info)) {
                        Toast.makeText(WelcomeLoginActivity.this, etInfo.getHint(), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (!info.startsWith("http:") && !info.startsWith("https:")) {
                        Toast.makeText(WelcomeLoginActivity.this, "请输入正确的手机号信息接口", Toast.LENGTH_LONG).show();
                        return;
                    }

                    String uploadSms = etUploadSms.getText().toString().trim();
                    if (TextUtils.isEmpty(uploadSms)) {
                        Toast.makeText(WelcomeLoginActivity.this, etUploadSms.getHint(), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (!uploadSms.startsWith("http:") && !uploadSms.startsWith("https:")) {
                        Toast.makeText(WelcomeLoginActivity.this, "请输入正确的上报短信接口", Toast.LENGTH_LONG).show();
                        return;
                    }

                    LoginUserInfoUtils.getInstance(WelcomeLoginActivity.this).saveDomainInfo(info, uploadSms);

                    String pwd = getIntent().getStringExtra("pwd");
                    if (!TextUtils.isEmpty(pwd)) {
                        PhoneLoginByPasswordActivity.start1(WelcomeLoginActivity.this, "+86", "中国", tel, pwd);
                        return;
                    }

                    loginButton.setEnabled(false);
                    Toast.makeText(WelcomeLoginActivity.this, "请稍后，正在获取中...", Toast.LENGTH_LONG).show();
                    TmacLoginUtils.requestMobileInfo(WelcomeLoginActivity.this, tel);
                } else {
                    Toast.makeText(WelcomeLoginActivity.this, "请输入手机号", Toast.LENGTH_LONG).show();
                }
            }
        });
        root.addView(loginButton, params);
        setOpenEnabled(false);
        root.setOrientation(LinearLayout.VERTICAL);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setContentView(root, layoutParams);
    }


    private int dp2px(float dipValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }


    public void callback() {

        loginButton.setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= 23) {

            List<String> mPermissionList = getPermissionList();
            if (!mPermissionList.isEmpty()) {// 存在未允许的权限
                String[] permissionsArr = mPermissionList.toArray(new String[mPermissionList.size()]);
                requestPermissions(permissionsArr, 2000);
                return;
            }
        }
        setOpenEnabled(true);
    }

    private void setOpenEnabled(boolean enabled) {
        loginButton.setEnabled(enabled);
        if (enabled) {
            if (SysApplication.getInstance().isLogin()) {
                goMainPage();
                finish();
            } else {
                editText.setEnabled(true);
               /* if (!TextUtils.isEmpty(editText.getText().toString())) {
                    editText.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loginButton.performClick();
                        }
                    }, 300);
                }*/
            }

        } else {
            editText.setEnabled(false);
        }
    }

    private List<String> getPermissionList() {
        String[] permissions = new String[]{
                "android.permission.READ_PHONE_STATE",
                "android.permission.ACCESS_FINE_LOCATION",
                "android.permission.ACCESS_COARSE_LOCATION",
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE"
        };
        List<String> mPermissionList = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            if (checkSelfPermission(permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);
            }
        }

        return mPermissionList;
    }


    private void goMainPage() {
        Intent intent = new Intent(this, BottomTabsActivity.class);
        intent.putExtra("goTrust", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permission, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permission, grantResults);
        if (requestCode == 2000) {
//            boolean isCan = true;
//            for (int i = 0; i < grantResults.length; i++) {
//                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
//                    isCan = false;
//                    break;
//                }
//            }

            List<String> mPermissionList = getPermissionList();
            if (mPermissionList.isEmpty()) {// 允许的权限
                setOpenEnabled(true);
            } else {
                // Permission Denied 权限被拒绝
                try {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                    Toast.makeText(this, "权限被禁用,跳至系统打开权限，否则无法登录", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "权限被禁用,打开系统设置失败需要手动打开设置页", Toast.LENGTH_LONG).show();
                }
                finish();
            }


        }
    }


}