package com.cmi.jegotrip.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cmi.jegotrip.ui.register.RegisterStep1Activity;

import java.util.ArrayList;
import java.util.List;

public class WelcomeRegActivity extends Activity {


    Button regButton;
    Button getTokenButton;
    EditText editText;
    MessageManager messageManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout root = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        root.setGravity(Gravity.CENTER);

        TextView tvVersion = new TextView(this);
        tvVersion.setText("当前打包的版本日期：2020.9.25");
        tvVersion.setTextColor(Color.parseColor("#333333"));
        tvVersion.setTextSize(18f);
        root.addView(tvVersion, params);

        int dp13 = dp2px(13f);
        editText = new EditText(this);
        editText.setHint("请输入本机手机号码");
        editText.setInputType(EditorInfo.TYPE_CLASS_PHONE);
        editText.setPadding(dp13, dp13, dp13, dp13);
        editText.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        root.addView(editText, params);

        regButton = new Button(this);
        regButton.setText("注册");
        regButton.setPadding(dp13, dp13, dp13, dp13);
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegUserInfoUtils.getInstance(WelcomeRegActivity.this).saveReg(true);
                dealResult();
            }
        });
        root.addView(regButton, params);

        getTokenButton = new Button(this);
        getTokenButton.setText("使用");
        getTokenButton.setPadding(dp13, dp13, dp13, dp13);
        getTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = editText.getText().toString();
                if (TextUtils.isEmpty(content)) {
                    Toast.makeText(WelcomeRegActivity.this, "请输入手机号", Toast.LENGTH_LONG).show();
                    return;
                }
                if (content.length() != 11) {
                    Toast.makeText(WelcomeRegActivity.this, "请输入正确的手机号", Toast.LENGTH_LONG).show();
                    return;
                }

                RegUserInfoUtils.getInstance(WelcomeRegActivity.this).saveReg(false);
                if (TextUtils.isEmpty(RegUserInfoUtils.getInstance(WelcomeRegActivity.this).getPhoneNumber())) {
                    RegUserInfoUtils.getInstance(WelcomeRegActivity.this).savePhoneAndPwd(editText.getText().toString(), "00000011");
                }
                dealResult();
            }
        });
        root.addView(getTokenButton, params);
        setEnabled(false);
        root.setOrientation(LinearLayout.VERTICAL);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setContentView(root, layoutParams);
        messageManager = new MessageManager(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (messageManager != null) {
            messageManager.unregister();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= 23) {
            List<String> mPermissionList = getPermissionList();
            if (!mPermissionList.isEmpty()) {// 存在未允许的权限
                String[] permissionsArr = mPermissionList.toArray(new String[mPermissionList.size()]);
                requestPermissions(permissionsArr, 2000);
                return;
            }
        }
        setEnabled(true);
    }

    private List<String> getPermissionList() {
        String[] permissions = new String[]{
                "android.permission.READ_PHONE_STATE",
                "android.permission.READ_SMS",
                "android.permission.READ_CONTACTS",
                "android.permission.RECEIVE_SMS",
        };
        List<String> mPermissionList = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            if (checkSelfPermission(permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);
            }
        }
        return mPermissionList;
    }

    private void setEnabled(boolean enabled) {
        regButton.setEnabled(enabled);
        getTokenButton.setEnabled(enabled);
        if (enabled) {
            messageManager.getNewMessages();
            if (TextUtils.isEmpty(editText.getText().toString())) {
                TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                @SuppressLint("MissingPermission")
                String tel = tm.getLine1Number();
                if (!TextUtils.isEmpty(tel)) {
                    tel = tel.replace("+86", "");
                    editText.setText(tel);
                    editText.setSelection(tel.length());
                }
            }
        }

        if (TextUtils.isEmpty(editText.getText().toString()) && !TextUtils.isEmpty(RegUserInfoUtils.getInstance(WelcomeRegActivity.this).getPhoneNumber())) {
            editText.setText(RegUserInfoUtils.getInstance(WelcomeRegActivity.this).getPhoneNumber());
        }

    }

    private int dp2px(float dipValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    private void dealResult() {

        startActivity(new Intent(this, RegisterStep1Activity.class));
//      finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 2000) {
            List<String> mPermissionList = getPermissionList();
            if (mPermissionList.isEmpty()) {// 允许的权限
                setEnabled(true);
            } else {
                // Permission Denied 权限被拒绝
                try {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                    Toast.makeText(this, "权限被禁用,跳至系统打开权限，否则无法注册", Toast.LENGTH_LONG).show();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "权限被禁用,打开系统设置失败需要手动打开设置页", Toast.LENGTH_LONG).show();
                }
            }

        }
    }
}