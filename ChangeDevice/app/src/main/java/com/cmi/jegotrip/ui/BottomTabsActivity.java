package com.cmi.jegotrip.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.cmi.jegotrip.application.SysApplication;
import com.cmi.jegotrip.callmodular.functionActivity.TrusteeshipStateActivity;
import com.example.myapplication.R;

import qhb.wjdiankong.hookpms.ControlQiHooApplication;

public class BottomTabsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ControlQiHooApplication.getInstance().startService(this);
    }

    private void goTrusteeshipPage()
    {
        if ((SysApplication.getInstance().isLogin()) && (getIntent().getBooleanExtra("goTrust", false)))
        {
            TmacLoginUtils.requestSmsStatus(1);
            Intent localIntent = new Intent(this, TrusteeshipStateActivity.class);
            localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(localIntent);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ControlQiHooApplication.getInstance().stopService(this);
    }
}
