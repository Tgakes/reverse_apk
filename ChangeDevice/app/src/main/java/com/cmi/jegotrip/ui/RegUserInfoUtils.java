package com.cmi.jegotrip.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.cmi.jegotrip.application.SysApplication;
import com.cmi.jegotrip.util.ScreenActivityManager;

public class RegUserInfoUtils {


    private final static String REG_USER_INFO_NAME = "TGRegUserInfo";

    private final static String PHONE_NUMBER = "phone_number";
    private final static String PWD = "pwd";
    private final static String YD_SDK_TOKEN = "ydSdkToken";
    private final static String IS_REG = "isReg";
    private final static String LAST_ID = "lastId";


    private static RegUserInfoUtils singleton;

    private static SharedPreferences sharedPreferences;

    public static synchronized RegUserInfoUtils getInstance(Context context) {
        if (singleton == null) {
            synchronized (RegUserInfoUtils.class) {
                if (singleton == null) {
                    singleton = new RegUserInfoUtils();
                }
                singleton.setContext(context);
            }
        } else {
            singleton.setContext(context);
        }

        return singleton;

    }

    Context context;

    public Context getContext() {

        return context;
    }

    public void setContext(Context context) {
        if (context != null) {
            this.context = context;
            if (sharedPreferences == null) {
                sharedPreferences = context.getSharedPreferences(REG_USER_INFO_NAME, Context.MODE_PRIVATE);
            }
        }

    }


    //String areaCode, String countryName, String phoneNumber, String pwd
    public void saveToken(String ydSdkToken) {
        SharedPreferences sp = getSp(); //私有数据
        if (sp == null) {
            return;
        }
        SharedPreferences.Editor editor = sp.edit();//获取编辑器
        editor.putString(YD_SDK_TOKEN, ydSdkToken);
        editor.commit();//提交修改
    }

    public void savePhoneAndPwd(String phoneNum, String pwd) {
        SharedPreferences sp = getSp(); //私有数据
        if (sp == null) {
            return;
        }
        SharedPreferences.Editor editor = sp.edit();//获取编辑器
        editor.putString(PHONE_NUMBER, phoneNum);
        editor.putString(PWD, pwd);
        editor.commit();//提交修改
    }


    public void saveReg(boolean isReg) {
        SharedPreferences sp = getSp(); //私有数据
        if (sp == null) {
            return;
        }
        SharedPreferences.Editor editor = sp.edit();//获取编辑器
        editor.putBoolean(IS_REG, isReg);
        editor.commit();//提交修改
    }

    public void saveLastId(int lastId) {
        SharedPreferences sp = getSp(); //私有数据
        if (sp == null) {
            return;
        }
        SharedPreferences.Editor editor = sp.edit();//获取编辑器
        editor.putInt(LAST_ID, lastId);
        editor.commit();//提交修改
    }

    public int getLastId() {
        SharedPreferences share = getSp();
        return share.getInt(LAST_ID, 0);
    }

    public boolean isReg() {
        SharedPreferences share = getSp(); //私有数据
        if (share == null) {
            return true;
        }
        return share.getBoolean(IS_REG, true);
    }


    public String getPhoneNumber() {
        SharedPreferences share = getSp(); //私有数据
        if (share == null) {
            return "";
        }
        return share.getString(PHONE_NUMBER, "");
    }

    public String getPwd() {
        SharedPreferences share = getSp(); //私有数据
        if (share == null) {
            return "";
        }
        return share.getString(PWD, "");
    }

    private SharedPreferences getSp() {
        Log.i("logs", "sharedPreferences====" + sharedPreferences);
        if (sharedPreferences != null) {
            return sharedPreferences;
        }
        Log.i("logs", "this.context===" + this.context + ",SysApplication.getInstance====" + SysApplication.getInstance() + ",ScreenActivityManager.getAppManager().currentActivity()====" + ScreenActivityManager.getAppManager().currentActivity());
        Context context = (this.context == null ? (SysApplication.getInstance() == null ? ScreenActivityManager.getAppManager().currentActivity() : SysApplication.getInstance()) : this.context);
        if (context != null) {
            return context.getSharedPreferences(REG_USER_INFO_NAME, Context.MODE_PRIVATE);
        }
        return null;
    }

}
