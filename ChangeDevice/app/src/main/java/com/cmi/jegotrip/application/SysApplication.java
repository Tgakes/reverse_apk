package com.cmi.jegotrip.application;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.cmi.jegotrip.callmodular.functionActivity.TrusteeshipStateActivity;
import com.cmi.jegotrip.ui.LoginUserInfoUtils;
import com.cmi.jegotrip.util.LocalSharedPrefsUtil;
import com.cmi.jegotrip2.call.model.CalledStatus;

import qhb.wjdiankong.hookpms.CheckService;

public class SysApplication extends Application {

    private static SysApplication instance = null;


    public static synchronized SysApplication getInstance() {
        SysApplication sysApplication;
        synchronized (SysApplication.class) {
            if (instance == null) {
                instance = new SysApplication();
            }
            sysApplication = instance;
        }
        return sysApplication;
    }


    public boolean isLogin() {
        return true;
    }


    public String getDeviceId(Context context){

        return LoginUserInfoUtils.getInstance(context).getImeiCode();
    }

}
