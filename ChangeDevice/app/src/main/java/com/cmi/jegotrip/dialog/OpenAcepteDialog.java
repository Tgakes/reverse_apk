package com.cmi.jegotrip.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;


public class OpenAcepteDialog extends Dialog {

    TextView f;

    public OpenAcepteDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      autoClick();
    }

    private void autoClick() {
        f.postDelayed(new Runnable() {
            @Override
            public void run() {
                f.performClick();
            }
        },1000);
    }
}
