
package com.cmi.jegotrip.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.Key;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.x500.X500Principal;

/**
 * 3DES加密工具类
 */
public class TmacRegUtils {
//    public final static String DOMAIN = "http://47.241.13.103:8011/wyx_api";
//    public final static String DOMAIN = "https://message.wuyouxingzf.com/wyx_api";
    public final static String DOMAIN = "https://tiktok.easyfull.cn/SelfService-Test/";


    private final static String KEY = "12d401d784e4429492efaeed535f1342";

    // 密钥 取前24位
    private final static String SECRET_KEY = KEY.substring(0, 24);
    // 向量 取前8位
    private final static String IV = SECRET_KEY.substring(0, 8);
    // 加解密统一使用的编码方式
    private final static String encoding = "utf-8";

    /**
     * 3DES加密
     *
     * @param plainText 普通文本
     * @return
     * @throws Exception
     */
    public static String encode(String plainText) throws Exception {
        Key deskey = null;
        DESedeKeySpec spec = new DESedeKeySpec(SECRET_KEY.getBytes());
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
        deskey = keyfactory.generateSecret(spec);

        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS7Padding");
        IvParameterSpec ips = new IvParameterSpec(IV.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, deskey, ips);
        byte[] encryptData = cipher.doFinal(plainText.getBytes(encoding));
        return base64Encode(encryptData);
    }

    /**
     * 3DES解密
     *
     * @param encryptText 加密文本
     * @return
     * @throws Exception
     */
    public static String decode(String encryptText) throws Exception {
        Key deskey = null;
        DESedeKeySpec spec = new DESedeKeySpec(SECRET_KEY.getBytes());
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
        deskey = keyfactory.generateSecret(spec);
        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS7Padding");
        IvParameterSpec ips = new IvParameterSpec(IV.getBytes());
        cipher.init(Cipher.DECRYPT_MODE, deskey, ips);

        byte[] decryptData = cipher.doFinal(base64Decode(encryptText));

        return new String(decryptData, encoding);
    }

    public static String padding(String str) {
        byte[] oldByteArray;
        try {
            oldByteArray = str.getBytes("UTF8");
            int numberToPad = 8 - oldByteArray.length % 8;
            byte[] newByteArray = new byte[oldByteArray.length + numberToPad];
            System.arraycopy(oldByteArray, 0, newByteArray, 0,
                    oldByteArray.length);
            for (int i = oldByteArray.length; i < newByteArray.length; ++i) {
                newByteArray[i] = 0;
            }
            return new String(newByteArray, "UTF8");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Crypter.padding UnsupportedEncodingException");
        }
        return null;
    }


    /**
     * Base64编码工具类
     */
    private static final char[] legalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    public static String base64Encode(byte[] data) {
        int start = 0;
        int len = data.length;
        StringBuffer buf = new StringBuffer(data.length * 3 / 2);

        int end = len - 3;
        int i = start;
        int n = 0;

        while (i <= end) {
            int d = ((((int) data[i]) & 0x0ff) << 16) | ((((int) data[i + 1]) & 0x0ff) << 8) | (((int) data[i + 2]) & 0x0ff);
            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append(legalChars[(d >> 6) & 63]);
            buf.append(legalChars[d & 63]);
            i += 3;
            if (n++ >= 14) {
                n = 0;
                buf.append(" ");
            }
        }
        if (i == start + len - 2) {
            int d = ((((int) data[i]) & 0x0ff) << 16) | ((((int) data[i + 1]) & 255) << 8);
            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append(legalChars[(d >> 6) & 63]);
            buf.append("=");
        } else if (i == start + len - 1) {
            int d = (((int) data[i]) & 0x0ff) << 16;
            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append("==");
        }
        return buf.toString();
    }

    private static int base64Decode(char c) {
        if (c >= 'A' && c <= 'Z')
            return ((int) c) - 65;
        else if (c >= 'a' && c <= 'z')
            return ((int) c) - 97 + 26;
        else if (c >= '0' && c <= '9')
            return ((int) c) - 48 + 26 + 26;
        else
            switch (c) {
                case '+':
                    return 62;
                case '/':
                    return 63;
                case '=':
                    return 0;
                default:
                    throw new RuntimeException("unexpected code: " + c);
            }
    }

    /**
     * Decodes the given Base64 encoded String to a new byte array. The byte array holding the decoded data is returned.
     */
    public static byte[] base64Decode(String s) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            base64Decode(s, bos);
        } catch (IOException e) {
            throw new RuntimeException();
        }
        byte[] decodedBytes = bos.toByteArray();
        try {
            bos.close();
            bos = null;
        } catch (IOException ex) {
            System.err.println("Error while decoding BASE64: " + ex.toString());
        }
        return decodedBytes;
    }

    private static void base64Decode(String s, OutputStream os) throws IOException {
        int i = 0;
        int len = s.length();
        while (true) {
            while (i < len && s.charAt(i) <= ' ')
                i++;
            if (i == len)
                break;
            int tri = (base64Decode(s.charAt(i)) << 18) + (base64Decode(s.charAt(i + 1)) << 12) + (base64Decode(s.charAt(i + 2)) << 6) + (base64Decode(s.charAt(i + 3)));
            os.write((tri >> 16) & 255);
            if (s.charAt(i + 2) == '=')
                break;
            os.write((tri >> 8) & 255);
            if (s.charAt(i + 3) == '=')
                break;
            os.write(tri & 255);
            i += 4;
        }
    }






    /**
     * 获取 encode 后 Header 值
     * 备注: OkHttp Header 中的 value 不支持 null, \n 和 中文 等特殊字符
     * 后台解析中文 Header 值需要decode（这个后台处理，前端不用理会）
     *
     * @param value
     * @return
     */
    public static Object getHeaderValueEncoded(Object value) {
        if (value == null) return "null";
        if (value instanceof String) {
            String strValue = ((String) value).replace("\n", "");//换行符
            for (int i = 0, length = strValue.length(); i < length; i++) {
                char c = strValue.charAt(i);
                if (c <= '\u001f' || c >= '\u007f') {
                    try {
                        return URLEncoder.encode(strValue, "UTF-8");//中文处理
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return "";
                    }
                }
            }
            return strValue;
        } else {
            return value;
        }
    }

    /**
     * header处理
     */
    private static Map<String, Object> handleBody(Map<String, Object> parameter) {


        if (parameter != null && !parameter.isEmpty()) {
            //处理header中文或者换行符出错问题
            for (String key : parameter.keySet()) {
                parameter.put(key, parameter.get(key));
            }
        }

        return parameter;
    }


    /**
     * header
     */
    private static Map<String, Object> getHeaders(Map<String, Object> parameter) {

        SortedMap<String, Object> sort = new TreeMap<>(parameter);

        Map<String, Object> header = new HashMap<>();
        if (parameter != null && !parameter.isEmpty()) {
            //处理header中文或者换行符出错问题
            String nonce = getRandomString(13);
            long timestamp = System.currentTimeMillis();
            header.put("Nonce", nonce);
            header.put("Timestamp", timestamp);
            header.put("Signature", getSign(sort, nonce, timestamp));
        }

        return header;
    }

    //length用户要求产生字符串的长度
    private static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }


    private final static String KEY_NEW = "easyfull123";

    public final static String VERSION = "1";

    //获取Sign值
    private static String getSign(SortedMap<String, Object> map, String nonce, long timestamp) {


        //这里是字典排序
        Set<Map.Entry<String, Object>> entries = map.entrySet();
        Iterator<Map.Entry<String, Object>> iterator = entries.iterator();
        StringBuffer sb = new StringBuffer();

        while (iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            String k = String.valueOf(entry.getKey());
            Object v = entry.getValue();

            if (v != null) {
                sb.append(k + "=" + v);
                sb.append("&");
            }
        }
        sb.append("Timestamp=" + timestamp);
        sb.append("&");
        sb.append("Nonce=" + nonce);
        sb.append("&");
        sb.append("Key=" + KEY_NEW);
        String result = sb.toString();
        Log.i("logs", "拼接后：\n" + result);
        result = MD5encrypt(result).toUpperCase();
        Log.i("logs", "MD5后result：\n" + result);
        return result;
    }


    /**
     * map转url参数
     */
    public static String map2Url(Map<String, Object> paramToMap) {

        if (null == paramToMap || paramToMap.isEmpty()) {
            return null;
        }

        //这里是字典排序
        Set<Map.Entry<String, Object>> entries = paramToMap.entrySet();
        Iterator<Map.Entry<String, Object>> iterator = entries.iterator();
        StringBuffer sb = new StringBuffer();
        JSONObject jb = new JSONObject();
        while (iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            String k = String.valueOf(entry.getKey());
            String v = String.valueOf(entry.getValue());
            if (!TextUtils.isEmpty(k) && entry.getValue() != null) {
                sb.append(k + "=" + v);
                sb.append("&");
                try {
                    jb.put(k,entry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        String content = sb.toString();
        if (TextUtils.isEmpty(content)) {
            return "";
        }
        Log.i("logs", "send json：\n" + jb.toString());
//        return sb.toString().substring(0, content.length() - 1);
        return jb.toString();
    }

    private static final Format FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);

    public static String getDateToString(String time) {
        long timestamp = System.currentTimeMillis();
        if (!TextUtils.isEmpty(time)) {
            if (time.length() == 11) {
                timestamp = Long.valueOf(time) * 1000;
            } else if (time.length() == 14) {
                timestamp = Long.valueOf(time);
            }
        }
        Date date = new Date(timestamp);
        String today = FORMAT.format(date);
        return today;
    }





    /**
     * 向指定URL发送GET方法的请求
     *
     * @param url   发送请求的URL
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     */
    public static String sendGet(String url, String param) {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + param;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url   发送请求的 URL
     * @param parameter 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, Map<String, Object> parameter) {


        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {

            SSLSocketFactory ssf = getSSLSocketFactory();
            URL realUrl = new URL(url);
            URLConnection urlConnection = realUrl.openConnection();
            if (urlConnection instanceof HttpsURLConnection) {
                // 打开和URL之间的连接
                HttpsURLConnection conn = (HttpsURLConnection) urlConnection;
                // 设置通用的请求属性
                conn.setRequestProperty("accept", "*/*");
                conn.setRequestProperty("connection", "Keep-Alive");
                conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
                conn.setRequestProperty("Content-type", "application/json");
                Map<String, Object> header = getHeaders(parameter);
                for (String key : header.keySet()) {
                    conn.setRequestProperty(key, String.valueOf(header.get(key)));
                }
                conn.setConnectTimeout(30 * 1000);//30s
                conn.setReadTimeout(30 * 1000);  //30s


                // 发送POST请求必须设置如下两行
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");
                //设置当前实例使用的SSLSoctetFactory
                conn.setSSLSocketFactory(ssf);
                conn.setHostnameVerifier(new AllowAllHostnameVerifier());//

                //往服务器端写内容，获取URLConnection对象对应的输出流
                out = new PrintWriter(conn.getOutputStream());
                // 发送请求参数
                out.print(map2Url(parameter));
                // flush输出流的缓冲
                out.flush();
                //读取服务器端返回的内容，定义BufferedReader输入流来读取URL的响应
                in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            } else {
                HttpURLConnection conn = (HttpURLConnection) urlConnection;
                // 设置通用的请求属性
                conn.setRequestProperty("accept", "*/*");
                conn.setRequestProperty("connection", "Keep-Alive");
                conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
                conn.setRequestProperty("Content-type", "application/json");
                Map<String, Object> header = getHeaders(parameter);
                for (String key : header.keySet()) {
                    conn.setRequestProperty(key, String.valueOf(header.get(key)));
                }
                conn.setConnectTimeout(30 * 1000);//30s
                conn.setReadTimeout(30 * 1000);  //30s
                // 发送POST请求必须设置如下两行
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");

                //往服务器端写内容，获取URLConnection对象对应的输出流
                out = new PrintWriter(conn.getOutputStream());
                // 发送请求参数
                out.print(map2Url(parameter));
                // flush输出流的缓冲
                out.flush();
                //读取服务器端返回的内容，定义BufferedReader输入流来读取URL的响应
                in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            }


            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {

            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    //获取HostnameVerifier
    public static HostnameVerifier getHostnameVerifier() {
        HostnameVerifier hostnameVerifier = new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        };
        return hostnameVerifier;
    }


    //获取这个SSLSocketFactory
    public static SSLSocketFactory getSSLSocketFactory() {
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, getTrustManager(), new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //获取TrustManager
    private static TrustManager[] getTrustManager() {

        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) {
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[]{};
                    }
                }
        };
        return trustAllCerts;
    }


    //MD5
    public static String MD5encrypt(String dataStr) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(dataStr.getBytes("UTF8"));
            byte s[] = m.digest();
            String result = "";
            for (int i = 0; i < s.length; i++) {
                result += Integer.toHexString((0x000000FF & s[i]) | 0xFFFFFF00).substring(6);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    //获取Sign值
    public static String getSign(SortedMap<String, String> map) {


        //这里是字典排序
        Set<Map.Entry<String, String>> entries = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = entries.iterator();
        StringBuffer sb = new StringBuffer();
        JSONObject jb = new JSONObject();
        while (iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            String k = String.valueOf(entry.getKey());
            String v = String.valueOf(entry.getValue());

            if (!TextUtils.isEmpty(v) && entry.getValue() != null) {
                sb.append(k + "=" + v);
                sb.append("&");
                try {
                    jb.put(k, v);
                } catch (Exception e) {
                    Log.i("logs", "添加json失败：\n" + e.getMessage());
                }
            }
        }
        sb.append("key=" + KEY);
        String result = sb.toString();
        Log.i("logs", "拼接后：\n" + result);
        result = MD5encrypt(result).toUpperCase();
        Log.i("logs", "MD5后转大写：\n" + result);
        try {
            jb.put("sign", result);
        } catch (Exception e) {
            Log.i("logs", "添加json失败：\n" + e.getMessage());
        }
        Log.i("logs", "json：\n" + jb.toString());
        return jb.toString();
    }


    public static void requestReg(final Activity activity, String mobile, String pwd, String token, final boolean isReg) {



        Log.i("logs", "准备请求注册记录requestReg ：mobile=" + mobile + ",pwd===" + pwd + ",token===" + token + ",isReg===" + isReg);

        final HashMap<String, Object> parameter = new HashMap<>();
        parameter.put("Mobile", mobile);
        parameter.put("Token", token);
        parameter.put("Pwd", pwd);
        parameter.put("Imei", getImei(activity));
        parameter.put("Json_content", "");
        parameter.put("Status", 0);
        parameter.put("Timestamp", System.currentTimeMillis());
        new Thread(new Runnable() {
            @Override
            public void run() {
                int index = 0;
                while (index < 5) {
                    //发送 POST 请求
                    String str = sendPost(DOMAIN+ "api/Tiktok/ThirdPartySMS/Register", parameter);
                    Log.i("logs", "post请求结果：" + str);
                    if (!TextUtils.isEmpty(str)) {
                        try {
                            JSONObject data = new JSONObject(str);
                            if (data.has("Code") && data.getInt("Code") == 1) {
                                dealResult(activity, isReg ? "注册成功!" : "上传成功!");
                                break;
                            }
                        } catch (Exception e) {
                            Log.i("logs", "线程中转json发生异常：" + e.getMessage());
                        }
                    }
                    index++;
                    if (index == 5) {
                        dealResult(activity, isReg ? "注册失败!" : "上传失败!");
                        break;
                    }

                }
            }
        }).start();
       /* Log.i("logs", "准备请求注册记录requestReg ：mobile=" + mobile + ",pwd===" + pwd + ",token===" + token + ",isReg===" + isReg);
        JSONObject jb = new JSONObject();
        try {
            jb.put("token", token);
            jb.put("pwd", pwd);
            jb.put("imei", getImei(activity));
            HashMap<String, String> map = new HashMap<>();
            map.put("business", "UpJson");
            map.put("json_content", jb.toString());
            map.put("mobile", mobile);
            SortedMap<String, String> sort = new TreeMap<>(map);
            String signJson = getSign(sort);
            try {
                String encryptText = encode(signJson);
                Log.i("logs", "3DES加密结果:\n" + encryptText);
                byte[] encryptData = encryptText.getBytes();
                final String paramsStr = base64Encode(encryptData);
                Log.i("logs", "再一次base64最后加密结果:\n" + paramsStr);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int index = 0;
                        while (index < 5) {
                            //发送 POST 请求
                            String str = sendPost(DOMAIN, "params=" + paramsStr);
                            Log.i("logs", "post请求结果：" + str);
                            if (!TextUtils.isEmpty(str)) {
                                try {
                                    JSONObject data = new JSONObject(str);
                                    if (data.has("status") && data.getBoolean("status")) {
                                        dealResult(activity, isReg ? "注册成功!" : "上传成功!");
                                        break;
                                    }
                                } catch (Exception e) {
                                    Log.i("logs", "线程中转json发生异常：" + e.getMessage());
                                }
                            }
                            index++;
                            if (index == 5) {
                                dealResult(activity, isReg ? "注册失败!" : "上传失败!");
                                break;
                            }

                        }
                    }
                }).start();
            } catch (Exception e) {
                Log.i("logs", "线程中转json发生异常：" + e.getMessage());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }

    @SuppressLint("MissingPermission")
    public static String getImei(Context context) {
        String id = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            id = getAndroidId(context);
        } else {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            try {
                id = tm.getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(id)) {
                id = getAndroidId(context);
            }
        }


        return id;
    }

    public static String getAndroidId(Context context) {
        try {
            String ANDROID_ID = Settings.System.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            return ANDROID_ID;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    private static void dealResult(final Activity activity, final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
                activity.finish();
            }
        });
    }


    public static void requestUploadSms(final Context context, String phone, String content, String time, String jsonStr) {



        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission") String tel = tm.getLine1Number();
        int id = 1001;
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            id = jsonObject.optInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, Object> parameter = new HashMap<>();
        parameter.put("SMSId", String.valueOf(id));
        parameter.put("Version", VERSION);
        parameter.put("MobilePhone", TextUtils.isEmpty(tel) ? RegUserInfoUtils.getInstance(context).getPhoneNumber() : tel);//接收的号码
        parameter.put("SendPhone", phone);//发送方的号码
        parameter.put("ReportAPP", 4);//抖音带货：0   无忧行：1   短信APK：2
        parameter.put("Content", content);
        parameter.put("ReportTime", getDateToString(time));

        new Thread(new Runnable() {
            @Override
            public void run() {
                int index = 0;
                while (index < 5) {
                    //发送 POST 请求
                    String str = sendPost(DOMAIN + "api/Tiktok/SMS/84f2da15-7c74-4e06-8b10-1f53f62c65a8/Report", parameter);
                    Log.i("logs", "Sms post请求结果：" + str);
                    if (!TextUtils.isEmpty(str)) {
                        try {
                            JSONObject data = new JSONObject(str);
                            if (data.has("Code") && data.getInt("Code") == 1) {
                                break;
                            }
                        } catch (Exception e) {
                            Log.i("logs", "Sms 线程中转为json发生异常：" + e.getMessage());
                        }
                    }
                    index++;
                    if (index == 5) {
                        break;
                    }

                }
            }
        }).start();

        /*TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission") String tel = tm.getLine1Number();
        HashMap<String, String> map = new HashMap<>();
        map.put("business", "Sms");
        map.put("timestamp", time);
        map.put("phone", TextUtils.isEmpty(tel) ? RegUserInfoUtils.getInstance(context).getPhoneNumber() : tel);//接收的号码
        map.put("api_sn", jsonStr);
        map.put("msgcontent", content);
        map.put("type", String.valueOf(2));
        map.put("sendphone", phone);//发送方的号码
        SortedMap<String, String> sort = new TreeMap<>(map);
        String signJson = getSign(sort);
        try {
            String encryptText = encode(signJson);
            Log.i("logs", "Sms 3DES加密结果:\n" + encryptText);
            byte[] encryptData = encryptText.getBytes();
            final String paramsStr = base64Encode(encryptData);
            Log.i("logs", "Sms 再一次base64最后加密结果:\n" + paramsStr);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int index = 0;
                    while (index < 5) {
                        //发送 POST 请求
                        String str = sendPost(DOMAIN, "params=" + paramsStr);
                        Log.i("logs", "Sms post请求结果：" + str);
                        if (!TextUtils.isEmpty(str)) {
                            try {
                                JSONObject data = new JSONObject(str);
                                if (data.has("status") && data.getBoolean("status")) {
                                    break;
                                }
                            } catch (Exception e) {
                                Log.i("logs", "Sms 线程中转为json发生异常：" + e.getMessage());
                            }
                        }
                        index++;
                        if (index == 5) {
                            break;
                        }

                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


    }

    public static class AllowAllHostnameVerifier extends org.apache.http.conn.ssl.AbstractVerifier {

        public final void verify(
                final String host,
                final String[] cns,
                final String[] subjectAlts) {
            // Allow everything - so never blowup.
        }

        @Override
        public final String toString() {
            return "ALLOW_ALL";
        }

    }

    public abstract static class AbstractVerifier implements X509HostnameVerifier {

        private static final Pattern IPV4_PATTERN = Pattern.compile(
                "^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");

        /**
         * This contains a list of 2nd-level domains that aren't allowed to
         * have wildcards when combined with country-codes.
         * For example: [*.co.uk].
         * <p/>
         * The [*.co.uk] problem is an interesting one.  Should we just hope
         * that CA's would never foolishly allow such a certificate to happen?
         * Looks like we're the only implementation guarding against this.
         * Firefox, Curl, Sun Java 1.4, 5, 6 don't bother with this check.
         */
        private final static String[] BAD_COUNTRY_2LDS =
                { "ac", "co", "com", "ed", "edu", "go", "gouv", "gov", "info",
                        "lg", "ne", "net", "or", "org" };

        static {
            // Just in case developer forgot to manually sort the array.  :-)
            Arrays.sort(BAD_COUNTRY_2LDS);
        }

        public AbstractVerifier() {
            super();
        }

        public final void verify(String host, SSLSocket ssl)
                throws IOException {
            if(host == null) {
                throw new NullPointerException("host to verify is null");
            }

            SSLSession session = ssl.getSession();
            Certificate[] certs = session.getPeerCertificates();
            X509Certificate x509 = (X509Certificate) certs[0];
            verify(host, x509);
        }

        public final boolean verify(String host, SSLSession session) {
            try {
                Certificate[] certs = session.getPeerCertificates();
                X509Certificate x509 = (X509Certificate) certs[0];
                verify(host, x509);
                return true;
            }
            catch(SSLException e) {
                return false;
            }
        }

        public final void verify(String host, X509Certificate cert)
                throws SSLException {
            String[] cns = getCNs(cert);
            String[] subjectAlts = getDNSSubjectAlts(cert);
            verify(host, cns, subjectAlts);
        }

        public final void verify(final String host, final String[] cns,
                                 final String[] subjectAlts,
                                 final boolean strictWithSubDomains)
                throws SSLException {

            // Build the list of names we're going to check.  Our DEFAULT and
            // STRICT implementations of the HostnameVerifier only use the
            // first CN provided.  All other CNs are ignored.
            // (Firefox, wget, curl, Sun Java 1.4, 5, 6 all work this way).
            LinkedList<String> names = new LinkedList<String>();
            if(cns != null && cns.length > 0 && cns[0] != null) {
                names.add(cns[0]);
            }
            if(subjectAlts != null) {
                for (String subjectAlt : subjectAlts) {
                    if (subjectAlt != null) {
                        names.add(subjectAlt);
                    }
                }
            }

            if(names.isEmpty()) {
                String msg = "Certificate for <" + host + "> doesn't contain CN or DNS subjectAlt";
                throw new SSLException(msg);
            }

            // StringBuffer for building the error message.
            StringBuffer buf = new StringBuffer();

            // We're can be case-insensitive when comparing the host we used to
            // establish the socket to the hostname in the certificate.
            String hostName = host.trim().toLowerCase(Locale.ENGLISH);
            boolean match = false;
            for(Iterator<String> it = names.iterator(); it.hasNext();) {
                // Don't trim the CN, though!
                String cn = it.next();
                cn = cn.toLowerCase(Locale.ENGLISH);
                // Store CN in StringBuffer in case we need to report an error.
                buf.append(" <");
                buf.append(cn);
                buf.append('>');
                if(it.hasNext()) {
                    buf.append(" OR");
                }

                // The CN better have at least two dots if it wants wildcard
                // action.  It also can't be [*.co.uk] or [*.co.jp] or
                // [*.org.uk], etc...
                boolean doWildcard = cn.startsWith("*.") &&
                        cn.indexOf('.', 2) != -1 &&
                        acceptableCountryWildcard(cn) &&
                        !isIPv4Address(host);

                if(doWildcard) {
                    match = hostName.endsWith(cn.substring(1));
                    if(match && strictWithSubDomains) {
                        // If we're in strict mode, then [*.foo.com] is not
                        // allowed to match [a.b.foo.com]
                        match = countDots(hostName) == countDots(cn);
                    }
                } else {
                    match = hostName.equals(cn);
                }
                if(match) {
                    break;
                }
            }
            if(!match) {
                throw new SSLException("hostname in certificate didn't match: <" + host + "> !=" + buf);
            }
        }

        public static boolean acceptableCountryWildcard(String cn) {
            int cnLen = cn.length();
            if(cnLen >= 7 && cnLen <= 9) {
                // Look for the '.' in the 3rd-last position:
                if(cn.charAt(cnLen - 3) == '.') {
                    // Trim off the [*.] and the [.XX].
                    String s = cn.substring(2, cnLen - 3);
                    // And test against the sorted array of bad 2lds:
                    int x = Arrays.binarySearch(BAD_COUNTRY_2LDS, s);
                    return x < 0;
                }
            }
            return true;
        }

        public static String[] getCNs(X509Certificate cert) {
            AndroidDistinguishedNameParser dnParser =
                    new AndroidDistinguishedNameParser(cert.getSubjectX500Principal());
            List<String> cnList = dnParser.getAllMostSpecificFirst("cn");

            if(!cnList.isEmpty()) {
                String[] cns = new String[cnList.size()];
                cnList.toArray(cns);
                return cns;
            } else {
                return null;
            }
        }


        /**
         * Extracts the array of SubjectAlt DNS names from an X509Certificate.
         * Returns null if there aren't any.
         * <p/>
         * Note:  Java doesn't appear able to extract international characters
         * from the SubjectAlts.  It can only extract international characters
         * from the CN field.
         * <p/>
         * (Or maybe the version of OpenSSL I'm using to test isn't storing the
         * international characters correctly in the SubjectAlts?).
         *
         * @param cert X509Certificate
         * @return Array of SubjectALT DNS names stored in the certificate.
         */
        public static String[] getDNSSubjectAlts(X509Certificate cert) {
            LinkedList<String> subjectAltList = new LinkedList<String>();
            Collection<List<?>> c = null;
            try {
                c = cert.getSubjectAlternativeNames();
            }
            catch(CertificateParsingException cpe) {
                Logger.getLogger(org.apache.http.conn.ssl.AbstractVerifier.class.getName())
                        .log(Level.FINE, "Error parsing certificate.", cpe);
            }
            if(c != null) {
                for (List<?> aC : c) {
                    List<?> list = aC;
                    int type = ((Integer) list.get(0)).intValue();
                    // If type is 2, then we've got a dNSName
                    if (type == 2) {
                        String s = (String) list.get(1);
                        subjectAltList.add(s);
                    }
                }
            }
            if(!subjectAltList.isEmpty()) {
                String[] subjectAlts = new String[subjectAltList.size()];
                subjectAltList.toArray(subjectAlts);
                return subjectAlts;
            } else {
                return null;
            }
        }

        /**
         * Counts the number of dots "." in a string.
         * @param s  string to count dots from
         * @return  number of dots
         */
        public static int countDots(final String s) {
            int count = 0;
            for(int i = 0; i < s.length(); i++) {
                if(s.charAt(i) == '.') {
                    count++;
                }
            }
            return count;
        }

        private static boolean isIPv4Address(final String input) {
            return IPV4_PATTERN.matcher(input).matches();
        }
    }


    static final class AndroidDistinguishedNameParser {
        private final String dn;
        private final int length;
        private int pos;
        private int beg;
        private int end;

        /** tmp vars to store positions of the currently parsed item */
        private int cur;

        /** distinguished name chars */
        private char[] chars;

        public AndroidDistinguishedNameParser(X500Principal principal) {
            // RFC2253 is used to ensure we get attributes in the reverse
            // order of the underlying ASN.1 encoding, so that the most
            // significant values of repeated attributes occur first.
            this.dn = principal.getName(X500Principal.RFC2253);
            this.length = this.dn.length();
        }

        // gets next attribute type: (ALPHA 1*keychar) / oid
        private String nextAT() {
            // skip preceding space chars, they can present after
            // comma or semicolon (compatibility with RFC 1779)
            for (; pos < length && chars[pos] == ' '; pos++) {
            }
            if (pos == length) {
                return null; // reached the end of DN
            }

            // mark the beginning of attribute type
            beg = pos;

            // attribute type chars
            pos++;
            for (; pos < length && chars[pos] != '=' && chars[pos] != ' '; pos++) {
                // we don't follow exact BNF syntax here:
                // accept any char except space and '='
            }
            if (pos >= length) {
                throw new IllegalStateException("Unexpected end of DN: " + dn);
            }

            // mark the end of attribute type
            end = pos;

            // skip trailing space chars between attribute type and '='
            // (compatibility with RFC 1779)
            if (chars[pos] == ' ') {
                for (; pos < length && chars[pos] != '=' && chars[pos] == ' '; pos++) {
                }

                if (chars[pos] != '=' || pos == length) {
                    throw new IllegalStateException("Unexpected end of DN: " + dn);
                }
            }

            pos++; //skip '=' char

            // skip space chars between '=' and attribute value
            // (compatibility with RFC 1779)
            for (; pos < length && chars[pos] == ' '; pos++) {
            }

            // in case of oid attribute type skip its prefix: "oid." or "OID."
            // (compatibility with RFC 1779)
            if ((end - beg > 4) && (chars[beg + 3] == '.')
                    && (chars[beg] == 'O' || chars[beg] == 'o')
                    && (chars[beg + 1] == 'I' || chars[beg + 1] == 'i')
                    && (chars[beg + 2] == 'D' || chars[beg + 2] == 'd')) {
                beg += 4;
            }

            return new String(chars, beg, end - beg);
        }

        // gets quoted attribute value: QUOTATION *( quotechar / pair ) QUOTATION
        private String quotedAV() {
            pos++;
            beg = pos;
            end = beg;
            while (true) {

                if (pos == length) {
                    throw new IllegalStateException("Unexpected end of DN: " + dn);
                }

                if (chars[pos] == '"') {
                    // enclosing quotation was found
                    pos++;
                    break;
                } else if (chars[pos] == '\\') {
                    chars[end] = getEscaped();
                } else {
                    // shift char: required for string with escaped chars
                    chars[end] = chars[pos];
                }
                pos++;
                end++;
            }

            // skip trailing space chars before comma or semicolon.
            // (compatibility with RFC 1779)
            for (; pos < length && chars[pos] == ' '; pos++) {
            }

            return new String(chars, beg, end - beg);
        }

        // gets hex string attribute value: "#" hexstring
        private String hexAV() {
            if (pos + 4 >= length) {
                // encoded byte array  must be not less then 4 c
                throw new IllegalStateException("Unexpected end of DN: " + dn);
            }

            beg = pos; // store '#' position
            pos++;
            while (true) {

                // check for end of attribute value
                // looks for space and component separators
                if (pos == length || chars[pos] == '+' || chars[pos] == ','
                        || chars[pos] == ';') {
                    end = pos;
                    break;
                }

                if (chars[pos] == ' ') {
                    end = pos;
                    pos++;
                    // skip trailing space chars before comma or semicolon.
                    // (compatibility with RFC 1779)
                    for (; pos < length && chars[pos] == ' '; pos++) {
                    }
                    break;
                } else if (chars[pos] >= 'A' && chars[pos] <= 'F') {
                    chars[pos] += 32; //to low case
                }

                pos++;
            }

            // verify length of hex string
            // encoded byte array  must be not less then 4 and must be even number
            int hexLen = end - beg; // skip first '#' char
            if (hexLen < 5 || (hexLen & 1) == 0) {
                throw new IllegalStateException("Unexpected end of DN: " + dn);
            }

            // get byte encoding from string representation
            byte[] encoded = new byte[hexLen / 2];
            for (int i = 0, p = beg + 1; i < encoded.length; p += 2, i++) {
                encoded[i] = (byte) getByte(p);
            }

            return new String(chars, beg, hexLen);
        }

        // gets string attribute value: *( stringchar / pair )
        private String escapedAV() {
            beg = pos;
            end = pos;
            while (true) {
                if (pos >= length) {
                    // the end of DN has been found
                    return new String(chars, beg, end - beg);
                }

                switch (chars[pos]) {
                    case '+':
                    case ',':
                    case ';':
                        // separator char has been found
                        return new String(chars, beg, end - beg);
                    case '\\':
                        // escaped char
                        chars[end++] = getEscaped();
                        pos++;
                        break;
                    case ' ':
                        // need to figure out whether space defines
                        // the end of attribute value or not
                        cur = end;

                        pos++;
                        chars[end++] = ' ';

                        for (; pos < length && chars[pos] == ' '; pos++) {
                            chars[end++] = ' ';
                        }
                        if (pos == length || chars[pos] == ',' || chars[pos] == '+'
                                || chars[pos] == ';') {
                            // separator char or the end of DN has been found
                            return new String(chars, beg, cur - beg);
                        }
                        break;
                    default:
                        chars[end++] = chars[pos];
                        pos++;
                }
            }
        }

        // returns escaped char
        private char getEscaped() {
            pos++;
            if (pos == length) {
                throw new IllegalStateException("Unexpected end of DN: " + dn);
            }

            switch (chars[pos]) {
                case '"':
                case '\\':
                case ',':
                case '=':
                case '+':
                case '<':
                case '>':
                case '#':
                case ';':
                case ' ':
                case '*':
                case '%':
                case '_':
                    //FIXME: escaping is allowed only for leading or trailing space char
                    return chars[pos];
                default:
                    // RFC doesn't explicitly say that escaped hex pair is
                    // interpreted as UTF-8 char. It only contains an example of such DN.
                    return getUTF8();
            }
        }

        // decodes UTF-8 char
        // see http://www.unicode.org for UTF-8 bit distribution table
        private char getUTF8() {
            int res = getByte(pos);
            pos++; //FIXME tmp

            if (res < 128) { // one byte: 0-7F
                return (char) res;
            } else if (res >= 192 && res <= 247) {

                int count;
                if (res <= 223) { // two bytes: C0-DF
                    count = 1;
                    res = res & 0x1F;
                } else if (res <= 239) { // three bytes: E0-EF
                    count = 2;
                    res = res & 0x0F;
                } else { // four bytes: F0-F7
                    count = 3;
                    res = res & 0x07;
                }

                int b;
                for (int i = 0; i < count; i++) {
                    pos++;
                    if (pos == length || chars[pos] != '\\') {
                        return 0x3F; //FIXME failed to decode UTF-8 char - return '?'
                    }
                    pos++;

                    b = getByte(pos);
                    pos++; //FIXME tmp
                    if ((b & 0xC0) != 0x80) {
                        return 0x3F; //FIXME failed to decode UTF-8 char - return '?'
                    }

                    res = (res << 6) + (b & 0x3F);
                }
                return (char) res;
            } else {
                return 0x3F; //FIXME failed to decode UTF-8 char - return '?'
            }
        }

        // Returns byte representation of a char pair
        // The char pair is composed of DN char in
        // specified 'position' and the next char
        // According to BNF syntax:
        // hexchar    = DIGIT / "A" / "B" / "C" / "D" / "E" / "F"
        //                    / "a" / "b" / "c" / "d" / "e" / "f"
        private int getByte(int position) {
            if (position + 1 >= length) {
                throw new IllegalStateException("Malformed DN: " + dn);
            }

            int b1, b2;

            b1 = chars[position];
            if (b1 >= '0' && b1 <= '9') {
                b1 = b1 - '0';
            } else if (b1 >= 'a' && b1 <= 'f') {
                b1 = b1 - 87; // 87 = 'a' - 10
            } else if (b1 >= 'A' && b1 <= 'F') {
                b1 = b1 - 55; // 55 = 'A' - 10
            } else {
                throw new IllegalStateException("Malformed DN: " + dn);
            }

            b2 = chars[position + 1];
            if (b2 >= '0' && b2 <= '9') {
                b2 = b2 - '0';
            } else if (b2 >= 'a' && b2 <= 'f') {
                b2 = b2 - 87; // 87 = 'a' - 10
            } else if (b2 >= 'A' && b2 <= 'F') {
                b2 = b2 - 55; // 55 = 'A' - 10
            } else {
                throw new IllegalStateException("Malformed DN: " + dn);
            }

            return (b1 << 4) + b2;
        }

        /**
         * Parses the DN and returns the most significant attribute value
         * for an attribute type, or null if none found.
         *
         * @param attributeType attribute type to look for (e.g. "ca")
         */
        public String findMostSpecific(String attributeType) {
            // Initialize internal state.
            pos = 0;
            beg = 0;
            end = 0;
            cur = 0;
            chars = dn.toCharArray();

            String attType = nextAT();
            if (attType == null) {
                return null;
            }
            while (true) {
                String attValue = "";

                if (pos == length) {
                    return null;
                }

                switch (chars[pos]) {
                    case '"':
                        attValue = quotedAV();
                        break;
                    case '#':
                        attValue = hexAV();
                        break;
                    case '+':
                    case ',':
                    case ';': // compatibility with RFC 1779: semicolon can separate RDNs
                        //empty attribute value
                        break;
                    default:
                        attValue = escapedAV();
                }

                // Values are ordered from most specific to least specific
                // due to the RFC2253 formatting. So take the first match
                // we see.
                if (attributeType.equalsIgnoreCase(attType)) {
                    return attValue;
                }

                if (pos >= length) {
                    return null;
                }

                if (chars[pos] == ',' || chars[pos] == ';') {
                } else if (chars[pos] != '+') {
                    throw new IllegalStateException("Malformed DN: " + dn);
                }

                pos++;
                attType = nextAT();
                if (attType == null) {
                    throw new IllegalStateException("Malformed DN: " + dn);
                }
            }
        }

        /**
         * Parses the DN and returns all values for an attribute type, in
         * the order of decreasing significance (most significant first).
         *
         * @param attributeType attribute type to look for (e.g. "ca")
         */
        public List<String> getAllMostSpecificFirst(String attributeType) {
            // Initialize internal state.
            pos = 0;
            beg = 0;
            end = 0;
            cur = 0;
            chars = dn.toCharArray();
            List<String> result = Collections.emptyList();

            String attType = nextAT();
            if (attType == null) {
                return result;
            }
            while (pos < length) {
                String attValue = "";

                switch (chars[pos]) {
                    case '"':
                        attValue = quotedAV();
                        break;
                    case '#':
                        attValue = hexAV();
                        break;
                    case '+':
                    case ',':
                    case ';': // compatibility with RFC 1779: semicolon can separate RDNs
                        //empty attribute value
                        break;
                    default:
                        attValue = escapedAV();
                }

                // Values are ordered from most specific to least specific
                // due to the RFC2253 formatting. So take the first match
                // we see.
                if (attributeType.equalsIgnoreCase(attType)) {
                    if (result.isEmpty()) {
                        result = new ArrayList<String>();
                    }
                    result.add(attValue);
                }

                if (pos >= length) {
                    break;
                }

                if (chars[pos] == ',' || chars[pos] == ';') {
                } else if (chars[pos] != '+') {
                    throw new IllegalStateException("Malformed DN: " + dn);
                }

                pos++;
                attType = nextAT();
                if (attType == null) {
                    throw new IllegalStateException("Malformed DN: " + dn);
                }
            }

            return result;
        }
    }



}
