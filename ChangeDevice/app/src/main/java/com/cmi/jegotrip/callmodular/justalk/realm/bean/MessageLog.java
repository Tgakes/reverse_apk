package com.cmi.jegotrip.callmodular.justalk.realm.bean;

public class MessageLog {


    private long _id;
    private String displayName;
    private String messageId;
    private String messageType;
    private boolean read;
    private String text;
    private long time;
    private String userId;


    public String getDisplayName() {
        return realmGet$displayName();
    }

    public String getMessageId() {
        return realmGet$messageId();
    }

    public String getMessageType() {
        return realmGet$messageType();
    }

    public String getText() {
        return realmGet$text();
    }

    public long getTime() {
        return realmGet$time();
    }

    public String getUserId() {
        return realmGet$userId();
    }

    public long get_id() {
        return realmGet$_id();
    }

    public boolean isRead() {
        return realmGet$read();
    }

    public long realmGet$_id() {
        return this._id;
    }

    public String realmGet$displayName() {
        return this.displayName;
    }

    public String realmGet$messageId() {
        return this.messageId;
    }

    public String realmGet$messageType() {
        return this.messageType;
    }

    public boolean realmGet$read() {
        return this.read;
    }

    public String realmGet$text() {
        return this.text;
    }

    public long realmGet$time() {
        return this.time;
    }

    public String realmGet$userId() {
        return this.userId;
    }

    public void realmSet$_id(long paramLong) {
        this._id = paramLong;
    }

    public void realmSet$displayName(String paramString) {
        this.displayName = paramString;
    }

    public void realmSet$messageId(String paramString) {
        this.messageId = paramString;
    }

    public void realmSet$messageType(String paramString) {
        this.messageType = paramString;
    }

    public void realmSet$read(boolean paramBoolean) {
        this.read = paramBoolean;
    }

    public void realmSet$text(String paramString) {
        this.text = paramString;
    }

    public void realmSet$time(long paramLong) {
        this.time = paramLong;
    }

    public void realmSet$userId(String paramString) {
        this.userId = paramString;
    }

    public void setDisplayName(String paramString) {
        realmSet$displayName(paramString);
    }

    public void setMessageId(String paramString) {
        realmSet$messageId(paramString);
    }

    public void setMessageType(String paramString) {
        realmSet$messageType(paramString);
    }

    public void setRead(boolean paramBoolean) {
        realmSet$read(paramBoolean);
    }

    public void setText(String paramString) {
        realmSet$text(paramString);
    }

    public void setTime(long paramLong) {
        realmSet$time(paramLong);
    }

    public void setUserId(String paramString) {
        realmSet$userId(paramString);
    }

    public void set_id(long paramLong) {
        realmSet$_id(paramLong);
    }
}
