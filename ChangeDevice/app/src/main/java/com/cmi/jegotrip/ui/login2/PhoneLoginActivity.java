package com.cmi.jegotrip.ui.login2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class PhoneLoginActivity extends Activity {

    public static void start(Context context) {
        Intent intent = new Intent(context, PhoneLoginActivity.class);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }

}
