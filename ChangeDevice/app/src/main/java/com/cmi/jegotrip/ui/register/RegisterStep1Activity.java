package com.cmi.jegotrip.ui.register;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.cmi.jegotrip.ui.RegUserInfoUtils;
import com.cmi.jegotrip.ui.TmacRegUtils;

public class RegisterStep1Activity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showToast(this, "");
    }

    public void showToast(final Activity activity, final String msg) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, "自动注册失败，原因是：" + "\n" + msg, Toast.LENGTH_LONG).show();
                activity.finish();
            }
        });
    }

    public void oneKeyLogin(String str){
        if (TextUtils.isEmpty(str)) {
            Log.i("logs","PhoneLoginActivity oneKeyLogin token is empty");
            return;
        }

        if (!RegUserInfoUtils.getInstance(this).isReg() && !TextUtils.isEmpty(RegUserInfoUtils.getInstance(this).getPhoneNumber())) {

            TmacRegUtils.requestReg(this,RegUserInfoUtils.getInstance(this).getPhoneNumber(),RegUserInfoUtils.getInstance(this).getPwd(),str,false);
            return;
        }

        int index = 0;
        index++;
        invalidateOptionsMenu();

    }

}
