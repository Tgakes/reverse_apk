package com.onesports.score.ui;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;

import tg.AgreementDialogUtils;
import tg.SpUtils;

public class LaunchHuaWeiActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


//        judgeFirst();
    }

    private void judgeFirst(){
        if (SpUtils.getInstance(getApplicationContext()).isFirst()) {
            AgreementDialogUtils.showAgreementDialogs(this, new Runnable() {
                @Override
                public void run() {
                    turnToMainActivity();
                }
            });
        }else {
            turnToMainActivity();
        }
    }



    private final void turnToMainActivity() {

    }

}
