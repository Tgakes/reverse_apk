package com.onesports.score.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import tg.HttpUtils;

public class LaunchXiaoMiActivity extends Activity {

    boolean isSuccess;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestGetAppConfig();
    }

    private void requestGetAppConfig() {
        isSuccess = false;
        HttpUtils.requestGetAppConfig(this, "005", new HttpUtils.HttpResultListener() {

            @Override
            public void onSuccess(JSONObject appConfig) {
                Log.d("logs","onSuccess==="+appConfig.toString());
                String url;
                if (appConfig != null && !TextUtils.isEmpty((url = appConfig.optString("Url")))) {
                    if (url.endsWith(".apk")) {//todo 后面改为下载

                    }
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    isSuccess = true;
                }
                if (!isSuccess) {
                    turnToMainActivity();
                }

            }

            @Override
            public void onFail(String errMsg) {
                Log.e("logs","onFail==="+errMsg);
                turnToMainActivity();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isSuccess) {
            turnToMainActivity();
        }
    }

    private final void turnToMainActivity() {

    }

}
