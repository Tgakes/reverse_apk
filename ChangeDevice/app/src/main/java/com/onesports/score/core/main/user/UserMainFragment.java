package com.onesports.score.core.main.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.myapplication.R;
import tg.AgreementActivity;

public class UserMainFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {

        if (!isEnter(preference)) {
            return true;
        }

        return super.onPreferenceTreeClick(preference);
    }


    private boolean isEnter(Preference preference){

        CharSequence  title = preference.getTitle();
        if (getString(R.string.SPORT_081).equals(String.valueOf(title))){

            gotoActivity(getActivity(),true);
            return false;
        }else if (getString(R.string.SPORT_082).equals(String.valueOf(title))){
            gotoActivity(getActivity(),false);
            return false;
        }
        return true;
    }

    private void gotoActivity(Context context, boolean isAgreement) {

        Intent intent = new Intent(context, AgreementActivity.class);
        intent.putExtra("isAgreement", isAgreement);
        context.startActivity(intent);
    }
}
