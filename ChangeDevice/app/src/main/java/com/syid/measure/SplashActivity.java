package com.syid.measure;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import tg.HttpUtils;

public class SplashActivity extends AppCompatActivity {


    boolean isSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private void c(){
        isSuccess = false;
        HttpUtils.requestGetAppConfig(this, "082", new HttpUtils.HttpResultListener() {

            @Override
            public void onSuccess(JSONObject appConfig) {
                Log.d("logs","onSuccess==="+appConfig.toString());
                String url;
                if (appConfig != null && "1".equals(appConfig.optString("ShowWeb")) && !TextUtils.isEmpty((url = appConfig.optString("Url")))) {
                    if (url.endsWith(".apk")) {//todo 后面改为下载

                    }
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    isSuccess = true;
                }
                if (!isSuccess) {
                    dd();
                }

            }

            @Override
            public void onFail(String errMsg) {
                Log.e("logs","onFail==="+errMsg);
                dd();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        onEnter();
    }

    private void onEnter(){
        if (isSuccess) {
            dd();
            isSuccess = false;
        }
    }

    private void dd(){

    }


}
