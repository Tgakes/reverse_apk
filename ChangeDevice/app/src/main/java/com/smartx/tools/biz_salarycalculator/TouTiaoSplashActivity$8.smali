.class Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity$8;
.super Ljava/lang/Object;
.source "TouTiaoSplashActivity.java"

# interfaces
.implements Ltg/HttpUtils$HttpResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;->requestGetAppConfig()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;


# direct methods
.method constructor <init>(Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity$8;->this$0:Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFail(Ljava/lang/String;)V
    .registers 5
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v0, "logs"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFail==="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iget-object v0, p0, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity$8;->this$0:Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;

    invoke-virtual {v0}, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;->l()V

    .line 54
    return-void
.end method

.method public onSuccess(Lorg/json/JSONObject;)V
    .registers 7
    .param p1, "appConfig"    # Lorg/json/JSONObject;

    .prologue
    .line 33
    const-string v2, "logs"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSuccess==="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    if-eqz p1, :cond_58

    const-string v2, "1"

    const-string v3, "ShowWeb"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_58

    const-string v2, "Url"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, "url":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_58

    .line 36
    const-string v2, ".apk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_40

    .line 39
    :cond_40
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity$8;->this$0:Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;

    const-class v3, Ltg/WebActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 40
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "url"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    iget-object v2, p0, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity$8;->this$0:Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;

    invoke-virtual {v2, v0}, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;->startActivity(Landroid/content/Intent;)V

    .line 42
    iget-object v2, p0, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity$8;->this$0:Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;->isSuccess:Z

    .line 44
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "url":Ljava/lang/String;
    :cond_58
    iget-object v2, p0, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity$8;->this$0:Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;

    iget-boolean v2, v2, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;->isSuccess:Z

    if-nez v2, :cond_63

    .line 45
    iget-object v2, p0, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity$8;->this$0:Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;

    invoke-virtual {v2}, Lcom/smartx/tools/biz_salarycalculator/TouTiaoSplashActivity;->l()V

    .line 48
    :cond_63
    return-void
.end method
