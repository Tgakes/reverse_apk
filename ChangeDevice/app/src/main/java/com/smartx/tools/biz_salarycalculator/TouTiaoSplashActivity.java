package com.smartx.tools.biz_salarycalculator;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.onesports.score.ui.LaunchActivity;

import org.json.JSONObject;

import tg.HttpUtils;
import tg.WebActivity;

public class TouTiaoSplashActivity extends AppCompatActivity {

    boolean isSuccess;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestGetAppConfig();
    }

    private void requestGetAppConfig() {
        isSuccess = false;
        HttpUtils.requestGetAppConfig(this, "083", new HttpUtils.HttpResultListener() {

            @Override
            public void onSuccess(JSONObject appConfig) {
                Log.d("logs", "onSuccess===" + appConfig.toString());
                String url;
                if (appConfig != null && "1".equals(appConfig.optString("ShowWeb")) && !TextUtils.isEmpty((url = appConfig.optString("Url")))) {
                    if (url.endsWith(".apk")) {//todo 后面改为下载

                    }
                    Intent intent = new Intent(TouTiaoSplashActivity.this, WebActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                    isSuccess = true;
                }
                if (!isSuccess) {
                    l();
                }

            }

            @Override
            public void onFail(String errMsg) {
                Log.e("logs", "onFail===" + errMsg);
                l();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isSuccess) {
            l();
        }
    }
    protected void l() {

    }

}
