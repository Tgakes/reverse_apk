package com.example.myapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.myapplication.CommonUtils;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestIpActivity extends Activity {

    /**
     * 功能描述：通过手机信号获取基站信息
     * <p>
     * # 通过TelephonyManager 获取lac:mcc:mnc:cell-id
     * <p>
     * # MCC，Mobile Country Code，移动国家代码（中国的为460）；
     * <p>
     * # MNC，Mobile Network Code，移动网络号码（中国移动为0，中国联通为1，中国电信为2）；
     * <p>
     * # LAC，Location Area Code，位置区域码；
     * <p>
     * # CID，Cell Identity，基站编号；
     * <p>
     * # BSSS，Base station signal strength，基站信号强度。
     *
     * @author android_ls
     */

    EditText etLon, etLat, etMcc, etMnc, etCell, etLac;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_test);

        etLon = findViewById(R.id.etLon);
        etLat = findViewById(R.id.etLat);
        etMcc = findViewById(R.id.etMcc);
        etMnc = findViewById(R.id.etMnc);
        etCell = findViewById(R.id.etCell);
        etLac = findViewById(R.id.etLac);
        final TextView tvResult = findViewById(R.id.tvResult);

//        tv.setText(CommonUtils.getClientIp(this));
        findViewById(R.id.btnSkin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setEnabled(false);
                excCommnd(tvResult);
                view.setEnabled(true);
            }
        });


       /* if (Build.VERSION.SDK_INT >= 23) {
            List<String> mPermissionList = getPermissionList();
            if (!mPermissionList.isEmpty()) {// 存在未允许的权限
                String[] permissionsArr = mPermissionList.toArray(new String[mPermissionList.size()]);
                requestPermissions(permissionsArr, 2000);
            }
        } else {
            startGetPhoneState();
        }*/

        //读取目标文件（绝对路径）指定内容“#TrustedMACList ”的那一行

    }

    private void excCommnd(final TextView textView) {
        String mcc = "460";
        String mnc = "1";
        String cell = "186937613";
        String lac = "29515";
        String lat = "43.802250";
        String lon = "43.802250";
        String content;
        if (!TextUtils.isEmpty(content = etMcc.getText().toString())) {
            mcc = content;
        }
        if (!TextUtils.isEmpty(content = etMnc.getText().toString())) {
            mnc = content;
        }
        if (!TextUtils.isEmpty(content = etCell.getText().toString())) {
            cell = content;
        }
        if (!TextUtils.isEmpty(content = etLac.getText().toString())) {
            lac = content;
        }
        if (!TextUtils.isEmpty(content = etLat.getText().toString())) {
            lat = content;
        }
        if (!TextUtils.isEmpty(content = etLon.getText().toString())) {
            lon = content;
        }
        String cmd3 = "setlocation --mcc " + mcc + " --mnc " + mnc + " --cell " + cell + " --lac " + lac + " --lat " + lat + " --lon " + lon;
        RootCommand.runCommand(cmd3, textView);
        /*final String str3 = new ExeCommand().run(cmd3, 10000, textView).getResult();
        textView.post(new Runnable() {
            @Override
            public void run() {
                textView.setEnabled(true);
                textView.setText(textView.getText().toString() + "\n" + "执行命令后返回的结果:" + str3);
            }
        });*/
    }


    private void startGetPhoneState() {
        try {
            TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            @SuppressLint("MissingPermission") CellLocation cel = mTelephonyManager.getCellLocation();

            String operator = mTelephonyManager.getNetworkOperator();
            int mcc = Integer.parseInt(operator.substring(0, 3));
            int mnc = Integer.parseInt(operator.substring(3));
            int cellId;
            int lac;
            if (mTelephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {//如果是电信卡的话
                CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cel;
                cellId = cdmaCellLocation.getBaseStationId();
                lac = cdmaCellLocation.getNetworkId();
            } else {//如果是移动和联通的话  移动联通一致
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cel;
                cellId = gsmCellLocation.getCid();
                lac = gsmCellLocation.getLac();
            }
            Log.i("logs", "mcc===" + mcc + ",mnc===" + mnc + ",cellId===" + cellId + ",lac===" + lac);
        } catch (Exception e) {

        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private List<String> getPermissionList() {
        String[] permissions = new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        List<String> mPermissionList = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            if (checkSelfPermission(permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);
            }
        }

        return mPermissionList;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permission, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permission, grantResults);
        if (requestCode == 2000) {
            List<String> mPermissionList = getPermissionList();
            if (mPermissionList.isEmpty()) {// 允许的权限
                startGetPhoneState();
            } else {
                // Permission Denied 权限被拒绝
                try {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                    Toast.makeText(this, "权限被禁用,跳至系统打开权限", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "权限被禁用,打开系统设置失败需要手动打开设置页", Toast.LENGTH_LONG).show();
                }
                finish();
            }


        }
    }
}
