package com.example.myapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import tg.HttpUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LocActivity extends Activity {

    TextView tvContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loc);

        tvContent = findViewById(R.id.tvContent);
       /* if (Build.VERSION.SDK_INT >= 23) {
            List<String> mPermissionList = getPermissionList();
            if (!mPermissionList.isEmpty()) {// 存在未允许的权限
                String[] permissionsArr = mPermissionList.toArray(new String[mPermissionList.size()]);
                requestPermissions(permissionsArr, 2000);
            }else {
                startGetPhoneState();
            }
        } else {
            startGetPhoneState();
        }*/
        HttpUtils.requestGetAppConfig(this, "005", new HttpUtils.HttpResultListener() {
            @Override
            public void onSuccess(JSONObject appConfig) {
                Log.i("logs","onSuccess appConfig==="+appConfig.toString());
            }

            @Override
            public void onFail(String errMsg) {
                Log.i("logs","onFail ==="+errMsg);
            }
        });
    }










    private void startGetPhoneState() {
        try {
            TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            @SuppressLint("MissingPermission") CellLocation cel = mTelephonyManager.getCellLocation();

            String operator = mTelephonyManager.getNetworkOperator();
            final int mcc = Integer.parseInt(operator.substring(0, 3));
            final int mnc = Integer.parseInt(operator.substring(3));
            final int cellId;
            final int lac;
            if (mTelephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {//如果是电信卡的话
                CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cel;
                cellId = cdmaCellLocation.getBaseStationId();
                lac = cdmaCellLocation.getNetworkId();
            } else {//如果是移动和联通的话  移动联通一致
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cel;
                cellId = gsmCellLocation.getCid();
                lac = gsmCellLocation.getLac();
            }
            Log.i("logs", "mcc===" + mcc + ",mnc===" + mnc + ",cellId===" + cellId + ",lac===" + lac);


            String content = "移动国家代码(mcc)：" + mcc + "\n" +
                    "移动网络号码(mnc)：" + mnc + "\n" +
                    "基站编号(cid)：" + cellId + "\n" +
                    "位置区域码(lac)：" + lac;
//            Toast.makeText(LocActivity.this, content, Toast.LENGTH_LONG).show();
            tvContent.setText(content);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("logs", e.getMessage());
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private List<String> getPermissionList() {
        String[] permissions = new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        List<String> mPermissionList = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            if (checkSelfPermission(permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);
            }
        }

        return mPermissionList;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permission, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permission, grantResults);
        if (requestCode == 2000) {
            List<String> mPermissionList = getPermissionList();
            if (mPermissionList.isEmpty()) {// 允许的权限
                startGetPhoneState();
            } else {
                // Permission Denied 权限被拒绝
                try {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                    Toast.makeText(this, "权限被禁用,跳至系统打开权限", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "权限被禁用,打开系统设置失败需要手动打开设置页", Toast.LENGTH_LONG).show();
                }
                finish();
            }


        }
    }
}
