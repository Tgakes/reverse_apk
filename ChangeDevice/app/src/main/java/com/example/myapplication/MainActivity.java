package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.cmi.jegotrip.ui.BottomTabsActivity;

import java.io.DataOutputStream;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        if (upgradeRootPermission(getPackageCodePath())) {
//            b(this);
//        }
        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        findViewById(R.id.btnSkin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, BottomTabsActivity.class));
            }
        });

        Log.i("logs","a===="+a(1));

    }






    public static String a(int index) {
        StringBuilder delete;
        Log.d("getLocalIPAddress", index + "");
        StringBuilder stringBuilder = new StringBuilder();
        String str = "";
        try {
            Enumeration networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration inetAddresses = ((NetworkInterface) networkInterfaces.nextElement()).getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    InetAddress inetAddress = (InetAddress) inetAddresses.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String hostAddress = inetAddress.getHostAddress();
                        if (!TextUtils.isEmpty(hostAddress) && hostAddress.contains("%")) {
                            hostAddress = hostAddress.substring(0, hostAddress.indexOf("%"));
                        }
                        if (index == 0 && (inetAddress instanceof Inet4Address)) {
                            //10.171.159.236
                            Log.d("getLocalIPv4Address:", "wifi private ip：" + hostAddress);
                            stringBuilder.append(hostAddress).append(",");
                        } else if (index != 1) {
                            continue;
                        } else if (inetAddress instanceof Inet6Address) {
                            Log.d("getLocalIPv6Address:", "wifi private ip：" + hostAddress);
                            stringBuilder.append(hostAddress).append(",");
                        }
                    }
                }
            }
            if (stringBuilder.length() > 1) {
                delete = stringBuilder.delete(stringBuilder.length() - 1, stringBuilder.length());
                return delete.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        delete = stringBuilder;
        return delete.toString();
    }



    private void b(Context context) {
        Exception e;
        Throwable th;
        //            ContentValues values = new ContentValues();
//            values.put("icc_id", "110110A51320F4004436");
//            values.put("sim_id", 1);
//            values.put("mcc", 8);
//            values.put("mnc", 464);
//            int update = context.getContentResolver().update(parse, values, null, null);
//            Log.e("UMCTelephonyManagement", "update-->" + update);

//        Log.e("UMCTelephonyManagement", "readSimInfoDbStart");
        Uri parse = Uri.parse("content://telephony/siminfo");
        Cursor query;
        try {
            query = context.getContentResolver().query(parse, new String[]{"_id", "icc_id", "sim_id", "mcc", "mnc"}, "sim_id>=?", new String[]{"0"}, null);
            if (query != null) {
                while (query.moveToNext()) {
                    try {
                        String string = query.getString(query.getColumnIndex("icc_id"));
                        int i = query.getInt(query.getColumnIndex("sim_id"));
                        int i2 = query.getInt(query.getColumnIndex("_id"));
                        String string2 = query.getString(query.getColumnIndex("mcc"));
                        String string3 = query.getString(query.getColumnIndex("mnc"));
                        /*if (this.c.o == -1 && this.c.p != -1 && this.c.p == i2) {
                            this.c.o = i;
                            Log.e("UMCTelephonyManagement", "通过读取sim db获取数据流量卡的卡槽值：" + i);
                        }
                        if (this.c.o == i) {
                            this.c.p = i2;
                        }
                        if (i == 0) {
                            this.c.a = string;
                            this.c.k = i2;
                            if (this.c.i == -1) {
                                this.c.i = i;
                            }
                            if (TextUtils.isEmpty(this.c.m) && !TextUtils.isEmpty(string)) {
                                this.c.m = string;
                            }
                        } else if (i == 1) {
                            this.c.b = string;
                            this.c.l = i2;
                            if (this.c.j == -1) {
                                this.c.j = i;
                            }
                            if (TextUtils.isEmpty(this.c.n) && !TextUtils.isEmpty(string)) {
                                this.c.n = string;
                            }
                        }*/
                        Log.i("UMCTelephonyManagement", "icc_id-->" + string);
                        Log.i("UMCTelephonyManagement", "sim_id-->" + i);
                        Log.i("UMCTelephonyManagement", "subId或者说是_id->" + i2);
                        Log.i("UMCTelephonyManagement", "mcc_string--->" + string3);
                        Log.i("UMCTelephonyManagement", "mnc_string--->" + string2);
                        Log.i("UMCTelephonyManagement", "---------------------------------");
                        break;
                    } catch (Exception e2) {
                        e = e2;
                        e.printStackTrace();
                    }

                }
            }
            if (query != null) {
                query.close();
            }
        } catch (Exception e3) {
            e = e3;
            query = null;
            try {
                e.printStackTrace();
                if (query != null) {
                    query.close();
                }
            } catch (Throwable th2) {
                if (query != null) {
                    query.close();
                }
            }
        } catch (Throwable th3) {
            th = th3;
            th3.printStackTrace();
            query = null;
            if (query != null) {
                query.close();
            }
            
        }
        
    }


    /**
     * 应用程序运行命令获取 Root权限，设备必须已破解(获得ROOT权限)
     *
     * @return 应用程序是/否获取Root权限
     */
    public static boolean upgradeRootPermission(String pkgCodePath) {
        Process process = null;
        DataOutputStream os = null;
        try {
            String cmd="chmod 777 " + pkgCodePath;
            process = Runtime.getRuntime().exec("su"); //切换到root帐号
            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(cmd + "\n");
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (Exception e) {
            return false;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                process.destroy();
            } catch (Exception e) {
            }
        }
        return true;
    }








}