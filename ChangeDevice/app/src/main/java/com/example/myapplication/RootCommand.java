package com.example.myapplication;

import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class RootCommand {
    public static void runCommand(final String command, final TextView textView) {
        final Handler handler = new Handler(Looper.getMainLooper());

        new Thread(new Runnable() {
            @Override
            public void run() {
                Process process = null;
                String result = "";
                DataOutputStream os = null;
                DataInputStream is = null;
                try {
                    process = Runtime.getRuntime().exec("su");
                    os = new DataOutputStream(process.getOutputStream());
                    is = new DataInputStream(process.getInputStream());
                    os.writeBytes(command + "\n");
                    os.writeBytes("exit\n");
                    os.flush();
                    String line = null;
                    while ((line = is.readLine()) != null) {
                        result += line;
                    }
                    process.waitFor();
                } catch (final IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(textView.getText().toString() + "\n" + "异常1：" + e.getMessage());
                        }
                    });
                    e.printStackTrace();
                } catch (final InterruptedException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(textView.getText().toString() + "\n" + "异常2：" + e.getMessage());
                        }
                    });
                    e.printStackTrace();
                } finally {
                    if (os != null) {
                        try {
                            os.close();
                        } catch (final IOException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    textView.setText(textView.getText().toString() + "\n" + "异常3：" + e.getMessage());
                                }
                            });
                            e.printStackTrace();
                        }
                    }
                    if (is != null) {
                        try {
                            is.close();
                        } catch (final IOException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    textView.setText(textView.getText().toString() + "\n" + "异常4：" + e.getMessage());
                                }
                            });
                            e.printStackTrace();
                        }
                    }
                    if (process != null) {
                        process.destroy();
                    }
                }
                final String finalResult = result;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(textView.getText().toString() + "\n" + "返回结果为：" + finalResult);
                        textView.setEnabled(true);
                    }
                });

            }
        }).start();


    }
}

