package com.mmmmg.jump.common;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mmmmg.jump.R;
import com.mmmmg.jump.activity.XieYiActivity;
import com.mmmmg.jump.activity.YinShiActivity;
import com.mmmmg.jump.utils.Constant;
import com.mmmmg.jump.utils.SPUtils;

/**
 * @Des 弹窗
 */
public class AgreementDialogCommon implements View.OnClickListener {

    private Context mContext;

    private BtnListener btnListener;

    private MaterialDialog.Builder dialog;
    private MaterialDialog ysmaterialDialog;
    private View view;

    public AgreementDialogCommon(Context context) {

        this.mContext = context;
        initDialog();
    }

    /**
     * @Des 初始化
     */
    private void initDialog() {
        dialog = new MaterialDialog.Builder(mContext);
        dialog.canceledOnTouchOutside(false);
        ysmaterialDialog = dialog.build();
        view = LayoutInflater.from(mContext).inflate(R.layout.dialog_yins, null);
        dialog.customView(view, false);
        ysmaterialDialog = dialog.show();
        ((TextView) ysmaterialDialog.findViewById(R.id.tv_title)).setText("用户协议隐私保护");
        ysmaterialDialog.findViewById(R.id.dialog_yins_cancel).setOnClickListener(this);
        ysmaterialDialog.findViewById(R.id.dialog_yins_sure).setOnClickListener(this);
        initDialogContent();
        ysmaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (btnListener != null) {
                    btnListener.isClose();
                }
            }
        });
        ysmaterialDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                if (btnListener != null) {
                    btnListener.isShow();
                }
            }
        });
    }

    /**
     * @Des 构造用户协议事件
     */
    private SpannableString setSpanXieYi() {

        SpannableString spanString = new SpannableString("《用户协议》");
        spanString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                //点击的响应事件
                mContext.startActivity(new Intent(mContext, XieYiActivity.class));
            }
        }, 0, spanString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spanString;
    }

    /**
     * @Des 构造隐私条例事件
     */
    private SpannableString setSpanYins() {

        SpannableString spanString2 = new SpannableString("《隐私条例》");
        spanString2.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                //点击的响应事件
                mContext.startActivity(new Intent(mContext, YinShiActivity.class));
            }
        }, 0, spanString2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spanString2;
    }

    /**
     * @Des 构造弹窗完整内容
     */
    private void initDialogContent() {

        TextView content = (TextView) ysmaterialDialog.findViewById(R.id.dialog_yins_content);
        content.setText("我们非常重视您的个人信息和隐私保护。为了更好的保障您的个人权益，在使用我们的产品前，请务必阅读");
        content.append(setSpanXieYi());
        content.append("与");
        content.append(setSpanYins());
        content.append("内容所有要款；确定即为同意。");
        content.setMovementMethod(LinkMovementMethod.getInstance());//开始响应点击事件
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dialog_yins_cancel) {
            if (btnListener != null) {
                btnListener.close(view);
            }
            ysmaterialDialog.dismiss();
            return;
        }
        if (view.getId() == R.id.dialog_yins_sure) {
            SPUtils.put(mContext, Constant.FIRST_USE, Boolean.valueOf(false));
            ysmaterialDialog.dismiss();
        }
    }

    public interface BtnListener {

        void close(View view);

        void ok(View view);

        void isShow();

        void isClose();
    }

    public void setBtnListener(BtnListener btnListener) {
        this.btnListener = btnListener;
    }
}
