
package com.mmmmg.jump.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.mmmmg.jump.R;

public class XieYiActivity extends FragmentActivity implements View.OnClickListener{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xie_yi);

        initView();
    }

    private void initView() {

        TextView title = findViewById(R.id.include_tool_title);
        title.setText("用户协议");

        findViewById(R.id.include_tool_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.include_tool_back){
            finish();
        }
    }
}
