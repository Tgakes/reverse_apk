package com.mmmmg.jump.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.mmmmg.jump.R;

public class AboutUsActivity extends FragmentActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {

        TextView title = findViewById(R.id.include_tool_title);
        title.setText("关于我们");

        findViewById(R.id.include_tool_back).setOnClickListener(this);
        findViewById(R.id.activity_about_us_xie_yi).setOnClickListener(this);
        findViewById(R.id.activity_about_us_yin_si).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        int i = view.getId();
        if (i == R.id.include_tool_back) {
            finish();
            return;
        }
        if(i == R.id.activity_about_us_xie_yi){
            Intent intent1 = new Intent(this, XieYiActivity.class);
            startActivity(intent1);
            return;
        }
        if(i == R.id.activity_about_us_yin_si){
            Intent intent2 = new Intent(this, YinShiActivity.class);
            startActivity(intent2);
            return;
        }
    }
}
