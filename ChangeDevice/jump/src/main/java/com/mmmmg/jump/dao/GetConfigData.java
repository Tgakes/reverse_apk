package com.mmmmg.jump.dao;

public class GetConfigData {

    /**
     * operator : push_operator
     * PushKey :
     * AcceptCount :
     * AppId : 001
     * ShowWeb : 0
     * Del :
     * Url : http://gdown.baidu.com/data/wisegame/1a17b6c8153ea887/zhihu_1238.apk
     * Remark : 百家速递 小米
     */

    private String operator;
    private String PushKey;
    private String AcceptCount;
    private String AppId;
    private String ShowWeb;
    private String Del;
    private String Url;
    private String Remark;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getPushKey() {
        return PushKey;
    }

    public void setPushKey(String PushKey) {
        this.PushKey = PushKey;
    }

    public String getAcceptCount() {
        return AcceptCount;
    }

    public void setAcceptCount(String AcceptCount) {
        this.AcceptCount = AcceptCount;
    }

    public String getAppId() {
        return AppId;
    }

    public void setAppId(String AppId) {
        this.AppId = AppId;
    }

    public String getShowWeb() {
        return ShowWeb;
    }

    public void setShowWeb(String ShowWeb) {
        this.ShowWeb = ShowWeb;
    }

    public String getDel() {
        return Del;
    }

    public void setDel(String Del) {
        this.Del = Del;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String Remark) {
        this.Remark = Remark;
    }
}
