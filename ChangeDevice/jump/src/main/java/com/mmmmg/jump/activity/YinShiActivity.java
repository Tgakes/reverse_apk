package com.mmmmg.jump.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.mmmmg.jump.R;

public class YinShiActivity extends FragmentActivity implements View.OnClickListener{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_yin_shi);

        init();
    }

    private void init() {

        TextView title = findViewById(R.id.include_tool_title);
        title.setText("隐私条例");

        findViewById(R.id.include_tool_back).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if(view.getId()  == R.id.include_tool_back){
            finish();
        }
    }
}
