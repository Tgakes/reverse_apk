package com.mmmmg.jump.network;

import com.mmmmg.jump.dao.GetConfigData;
import com.mmmmg.jump.dao.MaJiaResponse;
import com.mmmmg.jump.dao.NewsModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MajiaApiService {

    @GET("/getAppConfig")
    Call<MaJiaResponse<GetConfigData>> getConfig(@Query("appid") String appid);

    @GET("/toutiao/index")
    Call<NewsModel> getNews(@Query("type") String type, @Query("key") String appid);
}
