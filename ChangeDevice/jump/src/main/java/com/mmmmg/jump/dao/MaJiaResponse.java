package com.mmmmg.jump.dao;

public class MaJiaResponse<T> {

    /**
     * success : true
     * AppConfig : {"operator":"push_operator","PushKey":"","AcceptCount":"","AppId":"001","ShowWeb":"0","Del":"","Url":"http://gdown.baidu.com/data/wisegame/1a17b6c8153ea887/zhihu_1238.apk","Remark":"百家速递 小米"}
     */

    private boolean success;
    private T AppConfig;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getAppConfig() {
        return AppConfig;
    }

    public void setAppConfig(T appConfig) {
        AppConfig = appConfig;
    }
}
