package com.mmmmg.jump.network;

import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit.Builder;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MaJiaApiManager {
    private static MaJiaApiManager instance;
    private MajiaApiService apiService;

    public interface HttpCallback<T> {
        void onError(Throwable th);

        void onSuccess(T t);
    }

    private MaJiaApiManager() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(Level.HEADERS);
        interceptor.setLevel(Level.BODY);

        this.apiService = new Builder().baseUrl("http://47.105.39.248:8080")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MajiaApiService.class);
    }

    public MajiaApiService getApiService() {
        return this.apiService;
    }

    public static MaJiaApiManager getInstance() {
        if (instance == null) {
            synchronized (MaJiaApiManager.class) {
                if (instance == null) {
                    instance = new MaJiaApiManager();
                }
            }
        }
        return instance;
    }

    public <T> Subscription execute(Observable<T> observable, final HttpCallback<T> callback) {
        return observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<T>() {
                    public void onCompleted() {

                    }

                    public void onError(Throwable e) {
                        callback.onError(e);
                    }

                    public void onNext(T t) {
                        callback.onSuccess(t);
                    }
                });
    }
}
