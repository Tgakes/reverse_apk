package com.mmmmg.jump.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.Toast;

public class PackUtils {

    /**
     * @param appPackageName 需要卸载的app的包名
     */
    public static void uninstall(Activity activity, String appPackageName) {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_DELETE);
        intent.setData(Uri.parse("package:" + appPackageName));
        activity.startActivityForResult(intent, 0);
    }

    /**
     * @param appPackageName 需要启动的app的包名
     */
    public static void launch(Context context, String appPackageName) {

        PackageManager pm = context.getPackageManager();
        //启动页面的intent
        Intent intent = pm.getLaunchIntentForPackage(appPackageName);
        if (intent != null) {
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "找不到启动页面", Toast.LENGTH_SHORT).show();
            SPUtils.put(context, Constant.IS_APP, "");
        }
    }
}
