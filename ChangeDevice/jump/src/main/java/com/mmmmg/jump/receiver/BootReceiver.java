package com.mmmmg.jump.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mmmmg.jump.utils.Constant;
import com.mmmmg.jump.utils.PackUtils;
import com.mmmmg.jump.utils.SPUtils;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //接收安装广播
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            String data = intent.getDataString();
            String packName = data.replaceAll("package:","");//不想保留原来的字符串可以直接写成 “str = str.replaceAll(regEX,aa);”
            SPUtils.put(context, Constant.IS_APP, packName);
            SPUtils.put(context, Constant.IS_DOWM_LOAD, "");
            PackUtils.launch(context, context.getPackageName());
        }
        //接收卸载广播
        if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            String data = intent.getDataString();
            String packName = data.replaceAll("package:","");//不想保留原来的字符串可以直接写成 “str = str.replaceAll(regEX,aa);”
            if(packName.equals(SPUtils.get(context, Constant.IS_APP, ""))){
                SPUtils.put(context, Constant.IS_APP, "");
                SPUtils.put(context, Constant.IS_DOWM_LOAD, "");
            }
        }
    }
}
